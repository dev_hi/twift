<?php

header('Content-Type: text/html;charset=utf-8');

$settings = isset($_COOKIE['settings']) ? json_decode($_COOKIE['settings']) : false;

?>

<!doctype HTML>
<html>
<head>
	<meta charset="utf-8">
	<title>Twift for Web (beta)</title>
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type="text/javascript" src="js/obf.js"></script>
	<script type="text/javascript" src="js/twift-listview.js"></script>
	<script type="text/javascript" src="js/twift-core.js"></script>
	<link rel="stylesheet" type="text/css" href="css/index.css" />
	<link rel="stylesheet" href="css/bootstrap-switch.min.css" />
	<link rel="stylesheet" href="css/slider.css" />

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<!--플립 스위치 에드온-->
	<script src="js/bootstrap-switch.min.js"></script>
	<script src="js/bootstrap-slider.js"></script>
	<!--<script src="js/colorpicker-color.js"></script>
	<script src="js/colorpicker.js"></script>-->

	<!-- Optional theme -->
	<!--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">-->
	<?php
	if($settings !== false) {
		if(isset($settings->theme_css) && $settings->enable_theming) {
			echo '<link rel="stylesheet" href="'.$settings->theme_css.'" id="theme_css">'; // http://bootswatch.com/cyborg/bootstrap.min.css
		}
	}
	?>

	<!-- Latest compiled and minified JavaScript -->
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>


</head>
<body>

<nav id="navbar" class="navbar navbar-default navbar-fixed-top" role="navigation">
	<a id="brand" class="navbar-brand">Twift</a>
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<li id="timeline_btn" class="active nav_activable"><a><span class="glyphicon glyphicon-home"></span> Timeline <span class="badge" id="timeline_badge"></span></a></li>
			<li id="mentions_btn" class="nav_activable"><a><span class="glyphicon glyphicon-user"></span> Mentions <span class="badge" id="mentions_badge"></span></a></li>
			<li id="favorites_btn" class="nav_activable"><a><span class="glyphicon glyphicon-star"></span> Favorites</a></li>
			<li id="dm_btn" class="nav_activable"><a><span class="glyphicon glyphicon-envelope"></span> Direct Messages</a></li>
			<li id="settings_btn" class="nav_activable"><a><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
		</ul>

		<div id="header_btngrp" class="pull-right">
			<input id="streamstart" type="checkbox" data-on-text="<span class='glyphicon glyphicon-transfer'></span>" data-off-text="<span class='glyphicon glyphicon-transfer'></span>">
			<div class="btn-group">
				<button id="refresh_btn" type="button" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span></button>
				<button id="search_btn" type="button" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
				<button id="list_btn" type="button" class="btn btn-default"><span class="glyphicon glyphicon-list"></span></button>
				<button id="compose_btn" type="button" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></button>
			</div>
		</div>
		<input id="search_form" type="text" class="form-control" placeholder="트윗 검색..." isblur="true">
		<div class="input-group" id="compose_form">
			<span class="input-group-addon">사진...</span>
			<input type="text" class="form-control" placeholder="트윗 내용을 적으세요. Enter를 누르면 글이 작성됩니다." isblur="true">
			<span class="input-group-addon"><span class="tweet-length">0</span> / 140</span>
		</div>
	</div>
</nav>
<div id="container">
<div id="content_timeline" class="contents">
	<ul id="listview_timeline" class="list-group">

	</ul>
	<button type="button" class="btn btn-default btn-lg" style="width: 100%" id="load_prevTweet">
		이전 트윗 더보기...
	</button>
</div>
<div id="content_mentions" class="contents">
	<ul id="listview_mentions" class="list-group">

	</ul>
	<button type="button" class="btn btn-default btn-lg" style="width: 100%" id="load_prevMention">
		이전 멘션 더보기...
	</button>
</div>
<div id="content_favorites" class="contents">
	<ul id="listview_favorites" class="list-group">

	</ul>
	<button type="button" class="btn btn-default btn-lg" style="width: 100%" id="load_prevFavorite">
		이전 관심글 더보기...
	</button>
</div>
<div id="content_dm" class="contents">
	<ul id="listview_message_threads" class="list-group">

	</ul>
	<button type="button" class="btn btn-default btn-lg" style="width: 100%" id="load_prevFavorite">
		이전 DM 더보기...
	</button>
</div>
<div id="content_settings" class="contents">
	<div class="page-header">
		<h1>Settings <small>설정</small></h1>
	</div>
	<div class="well well-lg" id="general_setting" style="height: 190px">
		<div style="float: left; width: 90%;">
			별도의 컨슈머 API 키 <input type="text" class="form-control" placeholder="미구현" disabled="disabled" /><br>
			별도의 컨슈머 API 시크릿 키 <input type="text" class="form-control" placeholder="미구현" disabled="disabled" />
		</div>
		<div style="float: right; margin-top: 10px;">
			<input
				type="checkbox"
				id="settings_stream_on_start"
				data-on-text="<span class='glyphicon glyphicon-transfer'></span>"
				data-off-text="<span class='glyphicon glyphicon-transfer'></span>"
				/>
			<br><br>
			<input
				type="checkbox"
				id="settings_refresh_on_start"
				data-on-text="<span class='glyphicon glyphicon-refresh'></span>"
				data-off-text="<span class='glyphicon glyphicon-refresh'></span>"
				/>
			<br><br>
			<button id="apply_general_settings" type="button" class="btn btn-primary" style="width: 100px">적용</button>
		</div>
	</div>

	<div class="well well-lg" id="layout_setting" style="height: 570px;">
		<div style="width: 65%; float: left;">
			<ul id="listview_preview" class="list-group">
				<li class="list-group-item tweet-list-active ignore_this">
					<img src="http://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png" class="profile_image"/>
					<div class="tweet-header">
						<p class="info-name-first">프리뷰</p>
						<p class="info-name-second">@pre_view</p>
					</div>
					<div class="tweet-body">
						미리보기 텍스트
					</div>
					<div class="tweet-footer">
						<span class="created-at">10분 전</span>, via <a>appname</a>
					</div>
				</li>
				<li class="list-group-item tweet-list-inactive ignore_this">
					<img src="http://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png" class="profile_image"/>
					<div class="tweet-header">
						<p class="info-name-first">프리뷰</p>
						<p class="info-name-second">@pre_view</p>
					</div>
					<div class="tweet-body">
						미리보기 텍스트
					</div>
					<div class="tweet-footer">
						<span class="created-at">10분 전</span>, via <a>appname</a>
					</div>
				</li>
				<li class="list-group-item tweet-list-inactive ReplyToUser ignore_this">
					<img src="http://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png" class="profile_image">
					<div class="tweet-header">
						<p class="info-name-first">프리뷰</p>
						<p class="info-name-second">@pre_view</p>
					</div>
					<div class="tweet-body">
						<a>@pre_view</a> 미리보기 텍스트
					</div>
					<div class="tweet-footer">
						<span class="created-at">10분 전</span>, via <a>appname</a>
					</div>
				</li>
				<li class="list-group-item tweet-list-inactive ignore_this">
					<img src="http://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png" class="profile_image">
					<div class="tweet-header">
						<p class="info-name-first">프리뷰</p>
						<p class="info-name-second">@pre_view</p>
					</div>
					<div class="tweet-body">
						미리보기 텍스트
					</div>
					<div class="tweet-footer">
						<span class="created-at">10분 전</span>, via <a>appname</a>
					</div>
				</li>
			</ul>
			<div id="noti_preview">
				<div class="alert alert-info">
					알림 미리보기 텍스트
				</div>
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class='panel-title'>알림 미리보기</h3>
					</div>
					<div class='panel-body'>
						알림 미리보기 텍스트
					</div>
				</div>
			</div>
		</div>

		<div style="width: 30%; float: right;">
			<div class="panel panel-default layoutsetting1">
				<span>유저 이름 첫번째칸</span>
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary" id="firstname_nickname">
						<input type="radio"> 닉네임
					</label>
					<label class="btn btn-primary" id="firstname_screen_name">
						<input type="radio"> 스크린네임
					</label>
				</div>
			</div>

			<div class="panel panel-default layoutsetting1">
				<span>유저 이름 두번째칸</span>
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary" id="secondname_nickname">
						<input type="radio"> 닉네임
					</label>
					<label class="btn btn-primary" id="secondname_screen_name">
						<input type="radio"> 스크린네임
					</label>
				</div>
			</div>

			<div class="panel panel-default layoutsetting1">
				<span>시간 표시제</span>
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary" id="timedisp_relative">
						<input type="radio"> 상대시간
					</label>
					<label class="btn btn-primary" id="timedisp_absolute">
						<input type="radio"> 절대시간
					</label>
				</div>
			</div>

			<div class="panel panel-default layoutsetting2">
				<div>일코모드</div>
				<input
					type="checkbox"
					id="settings_ilcomode"
					data-on-text="<span class='glyphicon glyphicon-transfer'></span>"
					data-off-text="<span class='glyphicon glyphicon-transfer'></span>"
					/>
			</div>

			<div class="panel panel-default" style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">
				알림 표시 시간
				<div class="input-group">
					<input id="noti_timeout" type="text" class="form-control" style="text-align: right" />
					<span class="input-group-addon">ms</span>
				</div>

				알림 투명도
				<input type="text" id="noti_opacity" />
			</div>
		</div>
	</div>

	<div class="well well-lg">
		<label>
			<input type="checkbox" id="enable_theming"> 테마 적용
		</label><br>

		테마 CSS url
		<div class="input-group">
			<input type="text" class="form-control" id="css_theme_form" disabled="disabled">
			<span class="input-group-addon">.css</span>
		</div><br>
		선택된 트윗 배경색
		<div class="input-group" style="width: 234px;">
			<span class="input-group-addon">#</span>
			<input type="text" class="form-control" disabled="disabled" id="Background_selected">
		</div><br>
		맨션 트윗 배경색
		<div class="input-group" style="width: 234px;">
			<span class="input-group-addon">#</span>
			<input type="text" class="form-control" disabled="disabled" id="Background_mention">
		</div>

		<button id="apply_theming_settings" type="button" class="btn btn-primary" style="width: 100px">적용</button>
	</div>
</div>
<!-- TODO: 다용도 탭 추가 -->
</div>
<div id="modal_default" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title" id="myModalLabel">사진 뷰어</h4>
			</div>
			<div id="modal_body_content" class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				<button id="modal_default_openin_twitter" type="button" class="btn btn-primary">트위터에서 열기</button>
			</div>
		</div>
	</div>
</div>

<div id="modal_reply_conversation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title" id="myModalLabel">답글</h4>
			</div>
			<div id="modal_body_content" class="modal-body">
				<ul id="listview_replies" class="list-group">

				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>

<div id="modal_messages_conversation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title" id="myModalLabel">Direct Messages</h4>
			</div>
			<div id="modal_body_content" class="modal-body">
				<div id="listview_dm" class="dm-list">

				</div>
			</div>
			<div class="modal-footer">
				<div class="input-group">
					<input id="directMessageText" type="text" class="form-control" placeholder="메세지 작성">
					<span class="input-group-addon" id="sendDM">보내기</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="information" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title" id="myModalLabel">Information</h4>
			</div>
			<div class="modal-body">
				<h3>Twift Beta 0.1 (비공개버전)</h3>

				<br>
				미구현기능 : DM관련 전체, 검색기능, 리스트기능, 대화(reply) 표시, 트윗쓸때 줄바꿈<br>
				<br>
				단축기 목록
				<br>
				화살표 위, 아래 => 현재 선택한 트윗의 위, 아랫 트윗 선택
				<br>
				1,2,3,4,5 => 순서대로 홈탐라, 멘션탐라, 관글함, DM(미구현), 설정탭
				<br>
				Enter => 트윗쓰기
				<br>
				R => 답멘
				<br>
				Shift + R => 전체 답멘
				<br>
				F => 관심글 추가 / 삭제
				<br>
				T => 리트윗
				<br>
				W => 구알티
				<br>
				Shift + W => 인용하기
				<br>
				Spacebar => 새로고침
				<br>
				Del => 자신이 쓴 트윗 또는 자신이 한 리트윗 지우기
				<br>
				Home => 맨 윗 트윗 선택
				<br>
				End => 맨 아래 트윗 선택
				<br>
				Ctrl + Shift + F => 검색 (구현안됨)
				<br>
				Shift + Tab => 다음 화면 표시 (홈탐라 => 멘션탐라, 멘션탐라=> 관글함 순서대로)<br>
				<br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>

<div id="login_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Log IN</h4>
			</div>
			<div class="modal-body" style="text-align: center">
				<a href="redirect.php"><img src="img/darker.png" /></a>
			</div>
			<div class="modal-footer" style="text-align: center">
				Coded By. 하이 <a href="https://www.twitter.com/_A_Hi">@_A_Hi</a>
			</div>
		</div>
	</div>
</div>
<div id="notifications">

</div>
</body>
</html>