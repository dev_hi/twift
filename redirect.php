<?php

session_start();
require_once './init.php';

$object = new TwitterOAuth(CSM_KEY, CSM_SECRET);
$request_token = $object->getRequestToken(OA_CALLBACK);

$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

if($object->http_code == 200) {
	$url = $object->getAuthorizeURL($token);
	header('Location: '.$url);
}
else {
	echo 'Twitter Server Error :: HTTP Response Code is not 200';
}

?>