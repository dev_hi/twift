<?php

require_once '../init.php';
ini_set('max_execution_time', 0);

header('Content-Type: application/json;charset=UTF-8');

function isJsonValid($json) { //임시 json 검사함수
    if((count(explode('{', $json)) - 1) == (count(explode('}', $json)) - 1)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

$access_token = $_POST['access_token'];
$access_token_secret = $_POST['access_token_secret'];

if(empty($access_token) || empty($access_token_secret)) {
	die(json_encode(array(
		'event' => 'error',
		'message' => '토큰이 올바르지 않습니다. ?access_token = '.$access_token.'&access_token_secret = '.$access_token_secret
	)));
}

$object = new TwitterOAuth(CSM_KEY, CSM_SECRET,$access_token, $access_token_secret,TRUE);
$oAuthURL = $object->oAuthURL('user', 'GET', '');

$header = "GET /1.1/user.json?{$oAuthURL} HTTP/1.1\r\n";
$header .= "Host: userstream.twitter.com\r\n";
$header .= "Uesr-Agent: Mozilla/5.0\r\n\r\n";

//echo $header;

$rheader = '';

$socket = fsockopen("ssl://userstream.twitter.com", 443);

if($socket) {
	fwrite($socket, $header);
	while(!feof($socket)) {
		
		$ignor_char = '';

		while(!strpos($rheader, "\r\n\r\n")) {
			$rheader .= fgets($socket);
		}

		$json_flush = fgets($socket);

		if(!(strlen($json_flush) < 10)) {
			$json_splited = substr($json_flush, strpos($json_flush, '{'), strrpos($json_flush, '}') + 1);
			if(json_decode($json_splited) === null) {
				
			} else {
				echo $json_splited;
			}

			ob_flush();
			flush();
		} else {
			$ignor_char .= $json_flush;
		}

	}
}

//echo $rheader;
echo json_encode(array(
	'event' => 'stream end',
	'message' => '스트리밍이 종료되었습니다.'
));

fclose($socket);

?>