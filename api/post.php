<?php
/**
 * Created by PhpStorm.
 * User: 김하이
 * Date: 14. 3. 8
 * Time: 오전 3:56
 */

require_once '../init.php';


try {

	$access_token = $_POST['access_token'];
	$access_token_secret = $_POST['access_token_secret'];

	$endpoint = $_POST['endpoint'];
	$param = (array) json_decode($_POST['param']);

	$object = new TwitterOAuth(CSM_KEY, CSM_SECRET,$access_token, $access_token_secret,false);
	$result = $object->oAuthRequest($endpoint, 'POST', $param);

	//http_response_code($object->http_code);
	header('Content-Type: application/json;charset=UTF-8');

	print_r($result);

} catch (Exception $e) {
	die(json_encode(array(
		'errors' => array(
			'message' => '웹페이지 문법상 예외상황이 발생하였습니다.',
			'code' => $e->getCode()
		)
	)));
}
