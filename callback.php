<?php

session_start();
require_once './init.php';

if(isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) header('Location: destroy.php');

$object = new TwitterOAuth(CSM_KEY,CSM_SECRET,$_SESSION['oauth_token'],$_SESSION['oauth_token_secret']);
$tmp = $_SESSION['access_token_array'] = $object->getAccessToken($_REQUEST['oauth_verifier']);

unset($_SESSION['oauth_token']);
unset($_SESSION['oauth_token_secret']);

if($object->http_code == 200) {
	$_SESSION['status'] = 'verified';
	$_SESSION['access_token'] = $tmp['oauth_token'];
	$_SESSION['access_token_secret'] = $tmp['oauth_token_secret'];
	$_SESSION['user_id'] = $tmp['user_id'];

	header('Location: index.php');
} else {
	header('Location: destroy.php');
}

?>