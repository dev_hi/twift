<?php
/**
 * Created by PhpStorm.
 * User: 김하이
 * Date: 14. 3. 4
 * Time: 오전 3:09
 */

session_start();
header('Content-Type: application/json;charset=utf-8');

$status = $_SESSION['status'];
$access_token = $_SESSION['access_token'];
$access_token_secret = $_SESSION['access_token_secret'];
$userid = $_SESSION['user_id'];

if(!empty($access_token) || !empty($access_token_secret)) {
	die(json_encode(array(
		'event' => 'success',
		'message' => array(
			'access_token' => $access_token,
			'access_token_secret' => $access_token_secret,
			'user_id' => $userid
		)
	)));
} else {
	die(json_encode(array(
		'event' => 'error',
		'message' => '세션에 저장된 토큰이 없습니다. OAuth 인증을 해야합니다.'
	)));
}

?>