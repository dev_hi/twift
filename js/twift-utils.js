var utils = {
	getCookie : function (c_name) {
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++) {
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name) {
				return decodeURI(y);
			}
		}
		return false;
	},
	setCookie : function (c_name,value,exdays) {
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=encodeURI(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	},
	getRandomInt : function (min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	parseTimestampKorean : function (stamp) {
		var timeArray = stamp.split(' ');
		timeArray[3] = timeArray[3].split(':');

		var utc09;

		if(timeArray[3][0] >= 15) {
			utc09 = parseInt(timeArray[3][0]) - 15;
		} else {
			utc09 = parseInt(timeArray[3][0]) + 9;
		}

		var year = timeArray[5].toString() + '년';
		var month;
		var day;

		switch (timeArray[1]) {
			case 'Jan' :
				month = '1월';
				break;
			case 'Feb' :
				month = '2월';
				break;
			case 'Mar' :
				month = '3월';
				break;
			case 'Apr' :
				month = '4월';
				break;
			case 'May' :
				month = '5월';
				break;
			case 'Jun' :
				month = '6월';
				break;
			case 'Jul' :
				month = '7월';
				break;
			case 'Aug' :
				month = '8월';
				break;
			case 'Sep' :
				month = '9월';
				break;
			case 'Oct' :
				month = '10월';
				break;
			case 'Nov' :
				month = '11월';
				break;
			case 'Dec' :
				month = '12월';
				break;
		}

		switch (timeArray[0]) {
			case 'Sun' :
				day = '일요일';
				break;
			case 'Mon' :
				day = '월요일';
				break;
			case 'Tue' :
				day = '화요일';
				break;
			case 'Wed' :
				day = '수요일';
				break;
			case 'Thu' :
				day = '목요일';
				break;
			case 'Fri' :
				day = '금요일';
				break;
			case 'Sat' :
				day = '토요일';
				break;

		}

		var date = timeArray[2] + '일';
		var hour;
		var ampm;
		if(utc09 < 12) {
			ampm = '오전';
			hour = utc09 + '시';
		} else if(utc09 == 12) {
			ampm = '오후';
			hour = utc09 + '시';
		} else {
			ampm = '오후';
			hour = (utc09 - 12).toString() + '시';
		}
		var min = timeArray[3][1] + '분';
		var sec = timeArray[3][2] + '초';
		var whatisthis = timeArray[4];

		return {
			year: year,
			month: month,
			day: day,
			date: date,
			ampm: ampm,
			hour: hour,
			min: min,
			sec: sec
		};
	}
};