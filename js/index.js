/*
 TODO: MVC 패턴으로 재작성
 */

function Manager() {

	this.host = '.';
	this.isVerified = false;
	this.UserID = null;
	this.UserInfo = null;
	this.access_token = null;
	this.access_token_secret = null;
	this.friendsArray = [];

	this.isStreaming = false;
	this.lastTweet = 0;
	this.lastMention = 0;
	this.lastFavorite = 0;
	this.lastNotify = 0;
	this.enableKeyboardEvent = true;
	this.currentListView = null;
	this.currentReplyID = undefined;
	this.currentDirectID = undefined;
	this.currentMedia = undefined;
	this.content_mode = 0;
	this.timerID = [];
	this.settings = {};
	this.webkitNotiStatus = false;

	this.init = function () {

		var self = this;
		var settingsCookie = window.utils.getCookie('settings');

		if(!settingsCookie) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);

			var defaultSetting = {
				consumer_token: null,
				consumer_token_secret: null,

				ListView_NameFirst: 'nickname',
				ListView_NameSecond: 'screen_name',
				ListView_TimeDisplayType: 'relative',
				ListView_DisplayDate: false,

				ilco_profile_image_mode: false,

				noti_timeout: 5000,
				noti_parent_opacity: 0.9,

				stream_enable_on_start: true,
				refresh_all_on_start: true,

				apply_theming: false,
				theme_css: null,
				selectedColor: '#FBFBFB',
				replyToMeColor: '#D9EDF7'
			};

			window.utils.setCookie('settings', JSON.stringify(defaultSetting), exDate);
			this.settings = defaultSetting;

		} else {
			this.settings = JSON.parse(settingsCookie);
		}

		this.login();


		//리스트 관련

		//개별 ID Jquery 선택자

		this.brand = $('#brand');
		this.navbar = $('#navbar');
		this.timeline_btn = $('#timeline_btn');
		this.mentions_btn = $('#mentions_btn');
		this.favorites_btn = $('#favorites_btn');
		this.dm_btn = $('#dm_btn');
		this.settings_btn = $('#settings_btn');
		this.refresh_btn = $('#refresh_btn');
		this.streamstart = $('#streamstart');
		this.search_btn = $('#search_btn');
		this.list_btn = $('#list_btn');
		this.compose_btn = $('#compose_btn');
		this.content_timeline = $('#content_timeline');
		this.content_mentions = $('#content_mentions');
		this.content_favorites = $('#content_favorites');
		this.content_dm = $('#content_dm');
		this.content_settings = $('#content_settings');
		this.search_form = $('#search_form');
		this.compose_form = $('#compose_form');
		this.compose_form_input = this.compose_form.find('input');
		this.listview_timeline = $('#listview_timeline');
		this.listview_mentions = $('#listview_mentions');
		this.listview_favorites = $('#listview_favorites');
		this.listview_message_threads = $('#listview_message_threads');
		this.listview_replies = $('#listview_replies');
		this.listview_dm = $('#listview_dm');
		this.container = $('#container');
		this.notifications = $('#notifications');
		this.modal_default = $('#modal_default');
		this.modal_body_content = $('#modal_body_content');
		this.modal_default_openin_twitter = $('#modal_default_openin_twitter');
		this.information = $('#information');
		this.modal_messages_conversation = $('#modal_messages_conversation');
		this.directMessageText = $('#directMessageText');
		this.sendDM = $('#sendDM');

		this.load_prevTweet = $('#load_prevTweet');
		this.load_prevMention = $('#load_prevMention');
		this.load_prevFavorite = $('#load_prevFavorite');

		this.settings_stream_on_start = $('#settings_stream_on_start');
		this.settings_refresh_on_start = $('#settings_refresh_on_start');
		this.firstname_nickname = $('#firstname_nickname');
		this.firstname_screen_name = $("#firstname_screen_name");
		this.secondname_nickname = $('#secondname_nickname');
		this.secondname_screen_name = $('#secondname_screen_name');
		this.timedisp_relative = $('#timedisp_relative');
		this.timedisp_absolute = $('#timedisp_absolute');
		this.settings_ilcomode = $('#settings_ilcomode');
		this.noti_timeout = $('#noti_timeout');
		this.noti_opacity = $('#noti_opacity');
		this.css_theme_form = $('#css_theme_form');
		this.Background_selected = $('#Background_selected');
		this.Background_mention = $('#Background_mention');

		this.enable_theming = $('#enable_theming');
		this.apply_general_settings = $('#apply_general_settings');
		this.apply_theming_settings = $('#apply_theming_settings');

		this.timeline_badge = $('#timeline_badge');
		this.mentions_badge = $('#mentions_badge');

		//다중 class Jquery 선택자

		this.ClsSel = {
			contents: $('.contents'),
			nav_activable: $('.nav_activable')
		};

		//HTML 본문 오브젝트 초기화

		this.listviewObject_timeline = new ListView(this.listview_timeline, 'h','tweet-list-active', 'tweet-list-inactive');
		this.currentListView = this.listviewObject_timeline;
		this.listviewObject_mentions = new ListView(this.listview_mentions, 'm', 'tweet-list-active', 'tweet-list-inactive');
		this.listviewObject_favorites = new ListView(this.listview_favorites, 'f', 'tweet-list-active', 'tweet-list-inactive');
		this.listviewObject_messageThreads = new ListView(this.listview_message_threads, 'dt', 'thread-list-active', 'thread-list-inactive');
		this.listviewObject_replies = new ListView(this.listview_replies, 'r', 'tweet-list-active', 'tweet-list-inactive');
		this.listviewObject_dm = new ListView(this.listview_dm, 'dm', 'message-list-active', 'message-list-inactive');

		this.listviewObject_messageThreads.appendListView = function (direct_message) {

			var isActive = direct_message.sender.id_str == window.UserID;
			var conv_target = isActive ? direct_message.recipient : direct_message.sender;
			var self = this._this;
			var isNewConv = true;
			var returnObject = undefined;

			this.elementArray.forEach(function (value, key) {
				if(value.conv_target.id_str == conv_target.id_str) {
					isNewConv = false;
					returnObject = value;
					self.elementArray[key].messages.unshift(direct_message);
					self.elementArray[key].messages.sort(function(a,b) {
						return Date.parse(a.created_at) - Date.parse(b.created_at);
					});
				}
			});

			if(isNewConv) {
				returnObject = {
					conv_target:conv_target,
					messages: [direct_message]
				};

				this.elementArray.unshift(returnObject);
			}

			this.elementArray.sort(function(a,b) {
				var a_posix = Date.parse(a.messages[a.messages.length - 1].created_at);
				var b_posix = Date.parse(b.messages[b.messages.length - 1].created_at);
				return a_posix - b_posix;
			});

			return returnObject;
		};

		this.listviewObject_messageThreads.prependListView = function (direct_message) {

			var isActive = direct_message.sender.id_str == window.UserID;
			var conv_target = isActive ? direct_message.recipient : direct_message.sender;
			var self = this._this;
			var isNewConv = true;
			var returnObject = undefined;

			this.elementArray.forEach(function (value, key) {
				if(value.conv_target.id_str == conv_target.id_str) {
					isNewConv = false;
					returnObject = value;
					self.elementArray[key].messages.push(direct_message);
					self.elementArray[key].messages.sort(function(a,b) {
						return Date.parse(a.created_at) - Date.parse(b.created_at);
					});
				}
			});

			if(isNewConv) {
				returnObject = {
					conv_target:conv_target,
					messages: [direct_message]
				};

				this.elementArray.push(returnObject);
			}

			this.elementArray.sort(function(a,b) {
				var a_posix = Date.parse(a.messages[a.messages.length - 1].created_at);
				var b_posix = Date.parse(b.messages[b.messages.length - 1].created_at);
				return a_posix - b_posix;
			});

			return returnObject;
		};

		this.listviewObject_messageThreads.renderListView = function () {

			var elementArray = this.elementArray;
			this.initialize();
			this.elementArray = elementArray;
			var self = this._this;
			var returnArray = [];

			this.elementArray.forEach(function(value, key) {

				var conv_target = value.conv_target;
				var latestDirectMessage = value.messages[value.messages.length - 1];

				var time = window.utils.parseTimestampKorean(latestDirectMessage.created_at);
				var html = '<li class="list-group-item ' + self._listviewInactiveClassName + '" id="' + self._id_prefix + conv_target.id_str + '">' +
					'<div class="media">' +
					'<a class="pull-left" href="#">' +
					'<img class="media-object" src="' + conv_target.profile_image_url_https + '">' +
					'</a>' +
					'<div class="media-body">' +
					'<h4 class="media-heading">' + conv_target.name + ' <span class="thread-screenm">@' + conv_target.screen_name + '</span></h4>' +
					'<div class="thread-body">' +
					latestDirectMessage.text +
					'</div>' +
					'<span class="thread-time">' + time.year + ' ' + time.month + ' ' + time.date + ' ' + time.day + '</span>' +
					'<span class="glyphicon glyphicon-chevron-right arrow-icon"></span>' +
					'</div>' +
					'</div>' +
					'</li>';

				var createdObj = $(html).prependTo(self._target_object);
				returnArray.push(createdObj);

				self.elementArray[key].listview_jquery = createdObj;
				self.elementArray[key].listid = createdObj.attr('id');
			});

			return returnArray;
		};

		if(this.settings.ListView_TimeDisplayType == 'relative') {
			this.createRelativeTimer(this.listviewObject_timeline);
			this.createRelativeTimer(this.listviewObject_mentions);
			this.createRelativeTimer(this.listviewObject_favorites);
		}

		this.search_form.hide();
		this.compose_form.hide();
		this.content_mentions.hide();
		this.content_favorites.hide();
		this.content_dm.hide();
		this.content_settings.hide();
		this.streamstart.bootstrapSwitch();

		this.settings_stream_on_start.bootstrapSwitch('state', this.settings.stream_enable_on_start);
		this.settings_refresh_on_start.bootstrapSwitch('state', this.settings.refresh_all_on_start);
		this.settings_ilcomode.bootstrapSwitch('state', this.settings.ilco_profile_image_mode);

		if(this.settings.ListView_NameFirst == 'nickname') this.firstname_nickname.click();
		if(this.settings.ListView_NameFirst == 'screen_name') this.firstname_screen_name.click();
		if(this.settings.ListView_NameSecond == 'nickname') this.secondname_nickname.click();
		if(this.settings.ListView_NameSecond == 'screen_name') this.secondname_screen_name.click();
		if(this.settings.ListView_TimeDisplayType == 'relative') this.timedisp_relative.click();
		if(this.settings.ListView_TimeDisplayType == 'absolute') this.timedisp_absolute.click();

		this.noti_timeout.val(this.settings.noti_timeout);

		$('#noti_preview').css('opacity', (this.settings.noti_parent_opacity));

		this.settings_ilcomode.on('switchChange.bootstrapSwitch', function(e, data) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.ilco_profile_image_mode = data;
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
			self.refreshSettings();
		});


		var switch_sos = $('.bootstrap-switch-id-settings_stream_on_start');
		var switch_ros = $('.bootstrap-switch-id-settings_refresh_on_start');
		var switch_im = $('.bootstrap-switch-id-settings_ilcomode');

		switch_sos.attr('data-toggle',"tooltip");
		switch_sos.attr('data-placement',"bottom");
		switch_sos.attr('title',"시작시 스트리밍을 활성화합니다.");

		switch_ros.attr('data-toggle',"tooltip");
		switch_ros.attr('data-placement',"bottom");
		switch_ros.attr('title',"시작시 홈 타임라인, 멘션함, 관심글함을 불러옵니다.");

		switch_im.attr('data-toggle',"tooltip");
		switch_im.attr('data-placement',"bottom");
		switch_im.attr('title',"모든 프로필사진이 트위터 새알로 대체됩니다.");

		switch_sos.mouseover(function() {
			switch_sos.tooltip('show');
		});
		switch_sos.mouseout(function() {
			switch_sos.tooltip('hide');
		});
		switch_ros.mouseover(function() {
			switch_ros.tooltip('show');
		});
		switch_ros.mouseout(function() {
			switch_ros.tooltip('hide');
		});
		switch_im.mouseover(function() {
			switch_im.tooltip('show');
		});
		switch_im.mouseout(function() {
			switch_im.tooltip('hide');
		});

		this.modal_messages_conversation.on('hide.bs.modal', function () {
			self.currentDirectID = undefined;
			self.directMessageText.val('');
		});

		this.css_theme_form.val(this.settings.theme_css);
		this.Background_selected.val(this.settings.selectedColor.replace('#',''));
		this.Background_mention.val(this.settings.replyToMeColor.replace('#',''));

		$(document.head).append('<style type="text/css" id="tmp_css">' +
			'.tweet-list-active { background-color: ' + this.settings.selectedColor + '}' +
			'.ReplyToUser { background-color: ' + this.settings.replyToMeColor + '}' +
			'</style>');


		this.enable_theming.on('click', function(e) {
			var target = $(e.currentTarget);

			if(target.is(':checked')) {
				self.css_theme_form.removeAttr('disabled');
				//self.Background_selected.colorpicker('enable');
				//self.Background_mention.colorpicker('enable');
				self.Background_selected.removeAttr('disabled');
				self.Background_mention.removeAttr('disabled');
			} else if(!target.is(':checked')) {
				self.css_theme_form.attr('disabled', 'disabled');
				//self.Background_selected.colorpicker('disabled');
				//self.Background_mention.colorpicker('disabled');
				self.Background_selected.attr('disabled', 'disabled');
				self.Background_mention.attr('disabled', 'disabled');
			}
		});

		/*this.Background_selected.colorpicker({
		 format: 'hex',
		 color: this.settings.selectedColor
		 });

		 this.Background_mention.colorpicker({
		 format: 'hex',
		 color: this.settings.replyToMeColor
		 });*/


		this.enable_theming.prop('checked', this.settings.enable_theming);
		if(this.settings.enable_theming) {
			this.css_theme_form.removeAttr('disabled');
			//this.Background_selected.colorpicker('enable');
			//this.Background_mention.colorpicker('enable');
			this.Background_selected.removeAttr('disabled');
			this.Background_mention.removeAttr('disabled');
		}/* else if(!this.settings.enable_theming) {
		 this.Background_selected.colorpicker('disabled');
		 this.Background_mention.colorpicker('disabled');
		 }*/

		/*this.Background_selected.onclick(function(e) {
		 $(e.currentTarget).colorpicker('show');
		 });
		 this.Background_mention.onclick(function(e) {
		 $(e.currentTarget).colorpicker('show');
		 });*/

		this.apply_theming_settings.on('click', function() {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.enable_theming = self.enable_theming.is(':checked');
			self.settings.theme_css = self.css_theme_form.val();
			self.settings.selectedColor = (self.Background_selected.val().indexOf('#') == -1) ? '#' + self.Background_selected.val() : self.Background_selected.val();
			self.settings.replyToMeColor = (self.Background_mention.val().indexOf('#') == -1) ? '#' + self.Background_mention.val() : self.Background_mention.val();
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
			self.refreshSettings();
		});

		this.apply_general_settings.on('click', function(e) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.stream_enable_on_start = self.settings_stream_on_start.bootstrapSwitch('state');
			self.settings.refresh_all_on_start = self.settings_refresh_on_start.bootstrapSwitch('state');
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
		});
		this.firstname_nickname.on('click',function(e) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.ListView_NameFirst = 'nickname';
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
			self.refreshSettings();
		});
		this.firstname_screen_name.on('click',function(e) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.ListView_NameFirst = 'screen_name';
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
			self.refreshSettings();
		});
		this.secondname_nickname.on('click',function(e) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.ListView_NameSecond = 'nickname';
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
			self.refreshSettings();
		});
		this.secondname_screen_name.on('click',function(e) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.ListView_NameSecond = 'screen_name';
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
			self.refreshSettings();
		});
		this.timedisp_relative.on('click',function(e) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.ListView_TimeDisplayType = 'relative';
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
			self.refreshSettings();
		});
		this.timedisp_absolute.on('click',function(e) {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.ListView_TimeDisplayType = 'absolute';
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
			self.refreshSettings();
		});

		this.noti_timeout.on('blur', function() {
			var exDate = new Date();
			exDate.setDate(exDate.getDate() + 100);
			self.settings.noti_timeout = self.noti_timeout.val();
			window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);
		});

		this.noti_opacity.slider({
			min: 0,
			max: 100,
			step: 1,
			value: (this.settings.noti_parent_opacity * 100),
			formater: function(value) {
				$('#noti_preview').css('opacity', value/100);
				self.notifications.css('opacity', value/100);

				var exDate = new Date();
				exDate.setDate(exDate.getDate() + 100);
				self.settings.noti_parent_opacity = value/100;
				window.utils.setCookie('settings', JSON.stringify(self.settings), exDate);

				return value + '%';
			}
		});

		this.noti_opacity.parent().css('width','100%');

		//Click 이벤트 관리

		this.timeline_btn.click(function () { self.openContent(self.content_timeline,self.timeline_btn) });
		this.mentions_btn.click(function () { self.openContent(self.content_mentions,self.mentions_btn) });
		this.favorites_btn.click(function () { self.openContent(self.content_favorites,self.favorites_btn) });
		this.dm_btn.click(function () { self.openContent(self.content_dm,self.dm_btn) });
		this.settings_btn.click(function () { self.openContent(self.content_settings,self.settings_btn) });
		this.search_btn.click(function () {
			if(self.search_form.css('display') == 'none') {
				self.search_form.show();
				self.search_form.focus();
			} else {
				self.search_form.hide();
			}
		});
		this.list_btn.click(function () {
			//TODO: 리스트 기능 추가
		});
		this.compose_form_input.on('keyup', function (e) {
			var length_value = self.compose_form_input.val().length;
			var tweet_length = self.compose_form.find('.tweet-length');
			tweet_length.empty();
			tweet_length.append(length_value);
			if(length_value >= 140) {
				self.compose_form_input.addClass('overlimit');
			} else if(length_value < 140 && self.compose_form_input.hasClass('overlimit')) {
				self.compose_form_input.removeClass('overlimit');
			}
		});
		this.compose_btn.click(function () {
			if(self.compose_form.css('display') == 'none') {
				self.container.css('margin-top', (parseInt(self.container.css('margin-top')) + parseInt(self.compose_form.css('height'))));
				self.compose_form.show();
				self.compose_form_input.focus();
			} else {
				self.container.css('margin-top', (parseInt(self.container.css('margin-top')) - parseInt(self.compose_form.css('height'))));
				self.compose_form.hide();
			}
		});
		this.streamstart.on('switchChange.bootstrapSwitch', function (e, data) {
			if(data) {
				if(self.isStreaming) {
					console.warn('이미 스트리밍중입니다.');
				} else if(!self.isStreaming) {
					self.stream_start(self.access_token, self.access_token_secret);
				}
			} else if(!data) {
				self.stream_stop();
			}
		});
		this.refresh_btn.click(function () {
			if(self.content_mode == 0) {
				if(self.listviewObject_timeline.elementArray.length > 0) {
					self.getHomeTimeline(self.listviewObject_timeline.elementArray[self.listviewObject_timeline.elementArray.length - 1].id);
				} else {
					self.getHomeTimeline();
				}
			}
			if(self.content_mode == 1) {
				if(self.listviewObject_mentions.elementArray.length > 0) {
					self.getMentionsTimeline(self.listviewObject_mentions.elementArray[self.listviewObject_mentions.elementArray.length - 1].id);
				} else {
					self.getMentionsTimeline();
				}
			}
			if(self.content_mode == 2) {
				if(self.listviewObject_favorites.elementArray.length > 0) {
					self.getFavorites(self.listviewObject_favorites.elementArray[self.listviewObject_favorites.elementArray.length - 1].id);
				} else {
					self.getFavorites();
				}
			}
		});

		this.load_prevTweet.click(function() {
			self.getHomeTimeline(undefined, self.listviewObject_timeline.elementArray[0].id_str);
		});

		this.load_prevMention.click(function() {
			self.getMentionsTimeline(undefined, self.listviewObject_mentions.elementArray[0].id_str);
		});

		this.load_prevFavorite.click(function() {
			self.getFavorites(undefined, self.listviewObject_favorites.elementArray[0].id_str);
		});

		//focus / blur 이벤트 관리

		this.compose_form_input.focus(function () {
			self.compose_form_input.attr('isblur', 'false');
		});

		this.compose_form_input.blur(function () {
			self.compose_form_input.attr('isblur', 'true');
		});

		this.search_form.focus(function () {
			self.search_form.attr('isblur', 'false');
		});

		this.search_form.blur(function () {
			self.search_form.attr('isblur', 'true');
			self.search_form.hide();
		});


		//키 이벤트 바인딩
		$('body').keydown(function (e) {
			return self.keybordEventListener(e);
		});
	};

	//백엔드 파트 메소드

	this.useAPI = function(method, endpoint, param, callback) {
		var access_token = this.access_token;
		var access_token_secret = this.access_token_secret;
		var Parameters = typeof param !== 'undefined' ? JSON.stringify(param) : JSON.stringify({});

		$.ajax({
			url: this.host + '/api/' + method + '.php',
			method: 'post',
			data: {
				access_token: access_token,
				access_token_secret: access_token_secret,
				endpoint: endpoint,
				param: Parameters
			}
		}).done(callback).fail(callback);
	};

	this.stream_start = function(access_token, access_token_secret) {

		this.stream_XHR = new XMLHttpRequest();
		var param = 'access_token=' + access_token + '&access_token_secret=' + access_token_secret;
		var self = this;
		var tmp_old;

		self.stream_XHR.onreadystatechange = function () {

			if(self.stream_XHR.readyState == 1) {
				console.log('스트리밍 XHR 객체 초기화');
			}
			if(self.stream_XHR.readyState == 2) {
				self.isStreaming = true;
				self.createNotification('info', '유저 스트리밍이 시작되었습니다.', self.settings.noti_timeout);
				if(!self.streamstart.bootstrapSwitch('state')) self.streamstart.bootstrapSwitch('state', true);
			}
			if(self.stream_XHR.readyState == 3) {

				var tmp_latest = self.stream_XHR.responseText;
				tmp_latest = tmp_latest.replace(tmp_old, "");
				var parsed;

				try {
					parsed = JSON.parse(tmp_latest);

					if(typeof parsed.warning !== 'undefined') {
						console.warn(parsed.warning.code);
						alert(parsed.warning.message);
					} else if(typeof parsed.friends !== 'undefined') {
						self.friendsArray = parsed.friends;
					} else if(typeof parsed.event !== 'undefined') {
						self.tweetEventListener(parsed);
					} else if(typeof parsed.delete !== 'undefined') {
						self.deleteListView(parsed.delete.status);
					} else if(typeof parsed.direct_message !== 'undefined') {
						self.listviewObject_messageThreads.prependListView(parsed.direct_message);
						self.renderDirectMessage();
					} else {
						if(typeof parsed.text == 'undefined') {
							console.warn('json에서 text를 읽을수 없음');
							console.log(parsed);
						} else {
							if(parsed.entities.user_mentions.length !== 0) {
								parsed.entities.user_mentions.forEach(function (value) {
									if(value.id_str == self.UserID) {

										if(typeof parsed.retweeted_status !== 'undefined') {
											self.createNotificationPanel('primary', '@' + parsed.user.screen_name + '님이 당신의 글을 리트윗했습니다.', parsed.retweeted_status.text, 5000);
										} else {
											self.createNotificationPanel('primary', '@' + parsed.user.screen_name + '님이 멘션을 보냈습니다.', parsed.text, 5000);
										}
										self.addTweetListView(parsed, self.listviewObject_mentions, false);

										if(self.currentListView !== self.listviewObject_mentions) {
											var prev_badgeA = parseInt(self.mentions_badge.html());
											prev_badgeA = prev_badgeA.toString() == 'NaN' ? 0 : prev_badgeA;
											self.mentions_badge.empty();
											self.mentions_badge.append(prev_badgeA + 1);
										}
									}
								});
							}
							self.addTweetListView(parsed, self.listviewObject_timeline, false);
							if(self.currentListView !== self.listviewObject_timeline) {
								var prev_badgeA = parseInt(self.timeline_badge.html());
								prev_badgeA = prev_badgeA.toString() == 'NaN' ? 0 : prev_badgeA;
								self.timeline_badge.empty();
								self.timeline_badge.append(prev_badgeA + 1);
							}
						}
					}

				} catch(e) {
					console.error('Json을 파싱할수 없었습니다.');
					console.log(e);
				}

				tmp_old = self.stream_XHR.responseText;
			}
			if(self.stream_XHR.readyState == 4) {
				console.warn('스트리밍 정지됨.');
				self.isStreaming = false;
				self.createNotification('danger', '스트리밍이 정지되었습니다.', self.settings.noti_timeout);

				if(self.streamstart.bootstrapSwitch('state')) self.streamstart.bootstrapSwitch('state', false);
			}
		};

		this.stream_XHR.open("POST", this.host + '/api/user.php', true);
		this.stream_XHR.send(param);

	};



	this.stream_stop = function () {
		this.stream_XHR.abort();
	};

	this.tweetEventListener = function (obj) {
		var target = obj.target;
		var source = obj.source;
		var event = obj.event;
		var target_object = obj.target_object;
		var created_at = obj.created_at;

		var isActive = (this.UserID == source.id);
		var isPassive = (this.UserID == target.id);

		/*console.log(event);
		 console.log(target);
		 console.log(source);
		 console.log(target_object);
		 console.log('created_at=' + created_at);*/

		if(event == 'access_revoked') {
			this.createNotification('danger', '접근권한이 취소되었습니다.', this.settings.noti_timeout);
		}
		if(event == 'block') {
			this.createNotification('danger', target.name + '님을 블락하였습니다.', this.settings.noti_timeout);
		}
		if(event == 'unblock') {
			this.createNotification('info', target.name + '님을 언블락하였습니다.', this.settings.noti_timeout);
		}
		if(event == 'favorite') {
			if(isActive) {
				this.createNotificationPanel('warning', target.name + '님의 트윗을 관심글로 담았습니다.', target_object.text, this.settings.noti_timeout);
				this.listviewObject_timeline.elementArray.forEach(function(value) {
					if(value.real_id == target_object.id) {
						$(value.listview_jquery.find('.tweet-info')).append(' <span class="favorite-icon"><span class="glyphicon glyphicon-star"></span> 관심글</span>');
					}
				});

				this.listviewObject_mentions.elementArray.forEach(function(value) {
					if(value.real_id == target_object.id) {
						$(value.listview_jquery.find('.tweet-info')).append(' <span class="favorite-icon"><span class="glyphicon glyphicon-star"></span> 관심글</span>');
					}
				});

				this.addTweetListView(target_object, this.listviewObject_favorites, false);
			}
			else if(isPassive) this.createNotificationPanel('warning', source.name + '님이 당신의 트윗을 관심글로 담았습니다.', target_object.text, this.settings.noti_timeout);
		}
		if(event == 'unfavorite') {
			if(isActive) {
				this.createNotificationPanel('warning', target.name + '님의 트윗을 관심글에서 삭제했습니다.', target_object.text, this.settings.noti_timeout);
				this.deleteFavoriteListView(target_object);
			}
			else if(isPassive) this.createNotificationPanel('warning', source.name + '님이 당신의 트윗을 관심글에서 삭제했습니다.', target_object.text, this.settings.noti_timeout);
		}
		if(event == 'follow') {
			if(isActive) this.createNotification('info', target.name + '님을 팔로했습니다.', this.settings.noti_timeout);
			else if(isPassive) this.createNotification('info', source.name + '님이 당신을 팔로하였습니다.', this.settings.noti_timeout);
		}
		if(event == 'unfollow') {
			if(isActive) this.createNotification('info', target.name + '님을 언팔했습니다.', this.settings.noti_timeout);
			else if(isPassive) this.createNotification('info', source.name + '님이 당신을 언팔했습니다.', this.settings.noti_timeout);
		}
		if(event == 'list_created') {
			this.createNotification('info', '리스트를 만들었습니다.', this.settings.noti_timeout);
		}
		if(event == 'list_destroyed') {
			this.createNotification('danger', '리스트를 삭제했습니다.', this.settings.noti_timeout);
		}
		if(event == 'list_updated') {
			this.createNotification('info', '리스트가 업데이트 되었습니다.', this.settings.noti_timeout);
		}
		if(event == 'list_member_added') {
			if(isActive) this.createNotification('info', target.name + '님을 리스트에 추가하였습니다.', this.settings.noti_timeout);
			else if(isPassive) this.createNotification('info', source.name + '님이 당신을 리스트에 추가하였습니다.', this.settings.noti_timeout);
		}
		if(event == 'list_member_removed') {
			if(isActive) this.createNotification('danger', target.name + '님을 리스트에서 삭제하였습니다.', this.settings.noti_timeout);
			else if(isPassive) this.createNotification('danger', source.name + '님이 당신을 리스트에서 삭제하였습니다.', this.settings.noti_timeout);
		}
		if(event == 'list_user_subscribed') {
			this.createNotification('info', 'list_user_subscribed', this.settings.noti_timeout);
			console.log(obj);
		}
		if(event == 'user_update') {
			this.createNotification('info', 'user_update', this.settings.noti_timeout);
			console.log(obj);
		}
	};

	this.errorEventListener = function (error) {
		if(typeof error == 'string') {
			this.createNotification('danger', error, this.settings.noti_timeout);
		} else if(typeof error == 'object') {
			if(error.code == 37) {
				this.createNotification('danger', '해당 엔드포인트에 대한 권한이 없습니다. (' + error.message + ')', this.settings.noti_timeout);
			}
			if(error.code == 88) {
				this.createNotification('danger', '해당 엔드포인트에 대해서 리밋상태입니다. : ' + error.endpoint, this.settings.noti_timeout);
			}
			if(error.code == 139) {
				this.useAPI('post', 'favorites/destroy', {
					id: error.id
				}, function (data) {
					console.log(data);
				});
			}
		}
		console.error(error);
	};

	this.deleteListView = function (delete_status) {
		var found = false;
		var delete_target = null;
		var delete_key = null;

		this.listviewObject_timeline.elementArray.forEach(function (value, key) {
			if(value.id == delete_status.id_str) {
				found = true;
				delete_target = value.listview_jquery;
				delete_key = key;
			}
		});

		if(found) {
			delete_target.slideUp(undefined, function () {
				delete_target.remove();
			});
			this.listviewObject_mentions.elementArray.splice(delete_key, 1);
		}

		found = false;
		var delete_target2 = null;
		delete_key = null;

		this.listviewObject_mentions.elementArray.forEach(function (value, key) {
			if(value.id == delete_status.id_str) {
				found = true;
				delete_target2 = value.listview_jquery;
				delete_key = key;
			}
		});

		if(found) {
			delete_target2.slideUp(undefined, function () {
				delete_target2.remove();
			});
			this.listviewObject_mentions.elementArray.splice(delete_key, 1);
		}

		return found;
	};

	this.deleteFavoriteListView = function (delete_status) {
		var found = false;
		var delete_target = null;
		var delete_key = null;
		var delete_id = null;

		this.listviewObject_favorites.elementArray.forEach(function (value, key) {
			if(value.id == delete_status.id_str) {
				found = true;
				delete_target = value.listview_jquery;
				delete_key = key;
				delete_id = typeof value.retweeted_status == 'undefined' ? value.id_str : value.retweeted_status.id_str ;
			}
		});

		if(found) {
			delete_target.slideUp(undefined, function () {
				delete_target.remove();
			});
			this.listviewObject_favorites.elementArray.splice(delete_key, 1);
		}

		this.listviewObject_timeline.elementArray.forEach(function (value) {
			var id = typeof value.retweeted_status == 'undefined' ? value.id_str : value.retweeted_status.id_str ;
			if(id == delete_status.id_str) {
				value.listview_jquery.remove('.favorite-icon');
			}
		});
		this.listviewObject_mentions.elementArray.forEach(function (value) {
			var id = typeof value.retweeted_status == 'undefined' ? value.id_str : value.retweeted_status.id_str ;
			if(id == delete_status.id_str) {
				value.listview_jquery.remove('.favorite-icon');
			}
		});

		return found;
	};

	this.composeTweet = function (status, in_reply_to_status_id) {
		var self = this;

		if(status.length > 0) {
			this.useAPI('post', 'statuses/update', {
				status: status,
				in_reply_to_status_id: in_reply_to_status_id
			}, function(data) {
				if(!data) {
					self.createNotificationPanel('danger', '트윗을 쓸수 없었습니다.', status, self.settings.noti_timeout);
				} else {
					if(typeof data.errors !== 'undefined') {
						self.errorEventListener(data.errors);
					} else {
						self.createNotificationPanel('info', '트윗을 썼습니다.', data.text, self.settings.noti_timeout);
						self.currentReplyID = undefined;
					}
				}
			});
		}
	};

	this.sendDirectMessage = function (user_id, text) {
		this.useAPI('post', 'direct_messages/new', {
			user_id: user_id,
			text: text
		}, function(data) {
			if(!data) {
				self.createNotificationPanel('danger', '메세지를 보낼수 없었습니다.', text, self.settings.noti_timeout);
			} else {
				if(typeof data.errors !== 'undefined') {
					self.errorEventListener(data.errors);
				} else {
					self.createNotificationPanel('info', '메세지를 보냈습니다.', data.text, self.settings.noti_timeout);
					self.currentReplyID = undefined;
				}
			}
		});
	};

	this.searchTweet = function (text) {
		if(text.length > 0) {
			console.log('Tweet Search Dummy :: TextToSearch=' + text);
		}
	};

	this.favoriteTweet = function (id) {
		var self = this;

		this.useAPI('post', 'favorites/create', {
			id: id
		}, function(data) {
			if(typeof data.errors !== 'undefined') {
				data.errors.forEach(function(value) {
					value.id = id;
					self.errorEventListener(value);
				});
			}
		});
	};

	this.retweetTweet = function (id) {
		var self = this;

		if(confirm('리트윗 하시겠습니까?'))
			this.useAPI('post', 'statuses/retweet/' + id, undefined, function(data) {
				if(!data) {
					self.createNotification('danger', '리트윗을 할수 없었습니다.', self.settings.noti_timeout);
				} else {
					if(typeof data.errors !== 'undefined') {
						if(Object.prototype.toString.call(data.errors) == '[object Array]') {
							data.errors.forEach(function (value) {
								self.errorEventListener(value);
							});
						} else {
							self.errorEventListener(data.errors);
						}
					} else {
						self.createNotificationPanel('success', data.retweeted_status.user.name + '님의 트윗을 리트윗하였습니다.', data.retweeted_status.text, self.settings.noti_timeout);
					}
				}
			});
	};

	this.deleteTweet = function (id) {
		var self = this;

		if(confirm('정말로 삭제하시겠습니까?'))
			this.useAPI('post', 'statuses/destroy/' + id, undefined, function(data) {
				if(!data) {
					self.createNotification('danger', '삭제 할수 없었습니다.', self.settings.noti_timeout);
				} else {
					self.createNotificationPanel('danger', '삭제되었습니다.', data.text, self.settings.noti_timeout);
				}
			});
	};

	this.getHomeTimeline = function (since_id, max_id, count, include_entities) {
		var self = this;
		var isPrev = typeof max_id !== 'undefined';
		this.refresh_btn.button('loading');
		this.useAPI('get', 'statuses/home_timeline', {
			count: count,
			since_id: since_id,
			max_id: max_id,
			include_entities: include_entities
		}, function(data) {
			if(!data) {
				self.createNotification('danger', '홈 타임라인을 불러들일수 없었습니다.', self.settings.noti_timeout);
			} else if(data.length == 0) {
				self.createNotification('danger', '새로운 트윗이 없습니다.', self.settings.noti_timeout);
			} else if(typeof data.errors !== 'undefined') {
				data.errors.forEach(function(value) {
					value.endpoint = 'statuses/home_timeline.json';
					self.errorEventListener(value);
				});
			} else {
				if(isPrev) data.shift();
				if(!isPrev) data.reverse();
				data.forEach(function(value) {
					self.addTweetListView(value, self.listviewObject_timeline, isPrev);
				});
			}
			self.refresh_btn.empty();
			self.refresh_btn.append('<span class="glyphicon glyphicon-refresh"></span>');
			self.refresh_btn.removeAttr('disabled');
			self.refresh_btn.removeClass('disabled');
		});
	};

	this.getMentionsTimeline = function (since_id, max_id, count, include_entities) {
		var self = this;
		var isPrev = typeof max_id !== 'undefined';
		this.refresh_btn.button('loading');
		this.useAPI('get', 'statuses/mentions_timeline', {
			count: count,
			since_id: since_id,
			max_id: max_id,
			include_entities: include_entities
		}, function(data) {
			if(!data) {
				self.createNotification('danger', '멘션함을 불러들일수 없었습니다.', self.settings.noti_timeout);
			} else if(data.length == 0) {
				self.createNotification('danger', '새로운 멘션이 없습니다.', self.settings.noti_timeout);
			} else if(typeof data.errors !== 'undefined') {
				data.errors.forEach(function(value) {
					value.endpoint = 'statuses/mentions_timeline.json';
					self.errorEventListener(value);
				});
			} else {
				if(isPrev) data.shift();
				if(!isPrev) data.reverse();
				data.forEach(function(value) {
					self.addTweetListView(value, self.listviewObject_mentions, isPrev);
				});
			}
			self.refresh_btn.empty();
			self.refresh_btn.append('<span class="glyphicon glyphicon-refresh"></span>');
			self.refresh_btn.removeAttr('disabled');
			self.refresh_btn.removeClass('disabled');
		});
	};

	this.getFavorites = function (since_id, max_id, count, include_entities) {
		var self = this;
		var isPrev = typeof max_id !== 'undefined';
		this.refresh_btn.button('loading');
		this.useAPI('get', 'favorites/list', {
			count: count,
			since_id: since_id,
			max_id: max_id,
			include_entities: include_entities
		}, function(data) {
			if(!data) {
				self.createNotification('danger', '관심글함을 불러들일수 없었습니다.', self.settings.noti_timeout);
			} else if(data.length == 0) {
				self.createNotification('danger', '새로운 관심글이 없습니다.', self.settings.noti_timeout);
			} else if(typeof data.errors !== 'undefined') {
				data.errors.forEach(function(value) {
					value.endpoint = 'favorites/list.json';
					self.errorEventListener(value);
				});
			} else {
				if(isPrev) data.shift();
				if(!isPrev) data.reverse();
				data.forEach(function(value) {
					self.addTweetListView(value, self.listviewObject_favorites, isPrev);
				});
			}
			self.refresh_btn.empty();
			self.refresh_btn.append('<span class="glyphicon glyphicon-refresh"></span>');
			self.refresh_btn.removeAttr('disabled');
			self.refresh_btn.removeClass('disabled');
		});
	};

	this.renderDirectMessage = function () {

		var self = this;

		this.listviewObject_messageThreads.renderListView().forEach(function (createdObj) {
			createdObj.on('mouseover', function (e) {
				self.listviewObject_messageThreads.setActiveListView($(e.currentTarget));
			});

			createdObj.on('click', function () {
				self.listviewObject_dm.initialize();
				self.listviewObject_messageThreads.elementArray.forEach(function(value) {
					if(value.listid == createdObj.attr('id')) {
						value.messages.forEach(function (value) {
							self.addDirectMessageListView(value);
						});
						self.currentDirectID = value.conv_target.id_str;
					}
				});
				self.modal_messages_conversation.modal('show');
			});
		});

		if(typeof this.currentDirectID !== 'undefined') {
			this.listviewObject_dm.initialize();
			self.listviewObject_messageThreads.elementArray.forEach(function(value) {
				if(value.conv_target.id_str == self.currentDirectID) {
					value.messages.forEach(function (value) {
						self.addDirectMessageListView(value);
					});
				}
			});
		}

	};

	this.getMessages = function (since_id, max_id, count, include_entities) {
		var self = this;
		var isPrev = typeof max_id !== 'undefined';
		this.refresh_btn.button('loading');
		this.useAPI('get', 'direct_messages', {
			count: count,
			since_id: since_id,
			max_id: max_id,
			include_entities: include_entities
		}, function(data) {
			if(!data) {
				self.createNotification('danger', '메세지함을 불러들일수 없었습니다.', self.settings.noti_timeout);
			} else if(data.length == 0) {
				self.createNotification('danger', '새로운 메세지가 없습니다.', self.settings.noti_timeout);
			} else if(typeof data.errors !== 'undefined') {
				data.errors.forEach(function(value) {
					value.endpoint = 'direct_messages.json';
					self.errorEventListener(value);
				});
			} else {
				if(isPrev) data.shift();
				if(!isPrev) data.reverse();
				data.forEach(function(value) {
					if(isPrev) {
						self.listviewObject_messageThreads.appendListView(value);
					}
					if(!isPrev) {
						self.listviewObject_messageThreads.prependListView(value);
					}
					self.renderDirectMessage();
				});
			}
			self.refresh_btn.empty();
			self.refresh_btn.append('<span class="glyphicon glyphicon-refresh"></span>');
			self.refresh_btn.removeAttr('disabled');
			self.refresh_btn.removeClass('disabled');
		});
	};

	this.getSentMessages = function (since_id, max_id, count, include_entities) {
		var self = this;
		var isPrev = typeof max_id !== 'undefined';
		this.refresh_btn.button('loading');
		this.useAPI('get', 'direct_messages/sent', {
			count: count,
			since_id: since_id,
			max_id: max_id,
			include_entities: include_entities
		}, function(data) {
			if(!data) {
				self.createNotification('danger', '메세지함을 불러들일수 없었습니다.', self.settings.noti_timeout);
			} else if(data.length == 0) {
				self.createNotification('danger', '새로운 메세지가 없습니다.', self.settings.noti_timeout);
			} else if(typeof data.errors !== 'undefined') {
				data.errors.forEach(function(value) {
					value.endpoint = 'direct_messages.json';
					self.errorEventListener(value);
				});
			} else {
				if(isPrev) data.shift();
				if(!isPrev) data.reverse();
				data.forEach(function(value) {
					if(isPrev) {
						self.listviewObject_messageThreads.appendListView(value);
					}
					if(!isPrev) {
						self.listviewObject_messageThreads.prependListView(value);
					}
					self.renderDirectMessage();
				});
			}
			self.refresh_btn.empty();
			self.refresh_btn.append('<span class="glyphicon glyphicon-refresh"></span>');
			self.refresh_btn.removeAttr('disabled');
			self.refresh_btn.removeClass('disabled');
		});
	};

	this.login = function () {
		var self = this;
		var ajax = $.ajax( './getToken.php' );

		ajax.done(function (data) {

			if(data.event == 'success') {
				self.access_token = data.message.access_token;
				self.access_token_secret = data.message.access_token_secret;
				self.UserID = data.message.user_id;
				window.UserID = data.message.user_id;
				self.useAPI('get', 'users/show', {
					user_id:  data.message.user_id
				}, function (data) {
					self.UserInfo = data;
				});
				if(self.settings.stream_enable_on_start) {
					self.stream_start(self.access_token, self.access_token_secret);
				}
				if(self.settings.refresh_all_on_start) {
					self.getHomeTimeline();
					self.getMentionsTimeline();
					self.getFavorites();
					self.getMessages();
					self.getSentMessages();
				}

				self.information.modal({keybord:true, show: true});

			} else if(data.event == 'error') {
				$('#login_modal').modal({keybord:false, show: true});
				/*if(confirm(data.message)) {
				 window.location = './redirect.php';
				 } else {
				 alert('토큰이 없으므로 어플리케이션을 사용하실수 없습니다.');
				 }*/
			} else {
				window.location = './redirect.php';
			}
		}).fail(function () {
			window.location = './redirect.php';
		});
	};

	//프론트엔드 파트 메소드

	this.keybordEventListener = function(e) {
		if(this.enableKeyboardEvent) {
			if(e.keyCode == 27 && this.search_form.css('display') == 'block') {
				this.search_form.val('');
				this.search_form.hide();
				return false;
			}
			if(e.keyCode == 27 && this.compose_form.css('display') == 'table' && this.search_form.css('display') !== 'block') {
				this.compose_form_input.val('');
				this.container.css('margin-top', (parseInt(this.container.css('margin-top')) - parseInt(this.compose_form_input.css('height'))));
				this.compose_form.hide();
				return false;
			}
			if(this.compose_form.css('display') !== 'table' && this.search_form.css('display') !== 'block' ) {

				//Shortcut 1,2,3,4

				if(e.keyCode == 49) {
					this.openContent(this.content_timeline,this.timeline_btn);
					return false;
				}
				if(e.keyCode == 50) {
					this.openContent(this.content_mentions,this.mentions_btn);
					return false;
				}
				if(e.keyCode == 51) {
					this.openContent(this.content_favorites,this.favorites_btn);
					return false;
				}
				if(e.keyCode == 52) {
					this.openContent(this.content_dm,this.dm_btn);
					return false;
				}
				if(e.keyCode == 53) {
					this.openContent(this.content_settings,this.settings_btn);
					return false;
				}

				//Shortcut R

				if(e.keyCode == 82 && e.shiftKey) {
					this.openComposeWithReply(this.currentListView.elementArray[this.currentListView.currentActiveIndex].id, false);
					return false;
				}
				if(e.keyCode == 82) {
					this.openComposeWithReply(this.currentListView.elementArray[this.currentListView.currentActiveIndex].id, true);
					return false;
				}

				//Shortcut F, T, Spacebar

				if(e.keyCode == 70 && !e.shiftKey && !e.ctrlKey) {
					var favtweet_id = this.currentListView.elementArray[this.currentListView.currentActiveIndex].id;
					if(!this.currentListView.elementArray[this.currentListView.currentActiveIndex].deleted) this.favoriteTweet(favtweet_id);
					return false;
				}

				if(e.keyCode == 84) {
					var retweet_id = this.currentListView.elementArray[this.currentListView.currentActiveIndex].id;
					if(!this.currentListView.elementArray[this.currentListView.currentActiveIndex].deleted) this.retweetTweet(retweet_id);
					return false;
				}

				if(e.keyCode == 32) {
					this.getHomeTimeline(this.currentListView.elementArray[this.currentListView.elementArray.length - 1].id);
					return false;
				}

				//Shortcut w, Del

				if(e.keyCode == 87 && e.shiftKey) {
					var qtarget = this.currentListView.elementArray[this.currentListView.currentActiveIndex];
					if(!qtarget.deleted) this.quoteTweet(this.currentListView, qtarget.id_str);
					return false;
				}

				if(e.keyCode == 87 && !e.shiftKey) {
					var ltarget = this.currentListView.elementArray[this.currentListView.currentActiveIndex];
					if(!ltarget.deleted) this.legacyRetweet(this.currentListView, ltarget.id_str);
					return false;
				}

				if(e.keyCode == 46) {
					var delete_target = this.currentListView.elementArray[this.currentListView.currentActiveIndex];
					if(this.UserID == delete_target.user.id && !delete_target.deleted) {
						this.deleteTweet(delete_target.id_str);
						return false;
					}
				}

				//Shortcut home, end

				if(e.keyCode == 36) {
					this.currentListView.selectTop();
					//return false;
				}
				if(e.keyCode == 35) {
					this.currentListView.selectBottom();
					//return false;
				}

				//Shortcut arrow up, down

				if(e.keyCode == 38) {
					if(this.currentListView.currentActiveIndex == null) {
						this.currentListView.selectBottom();
						return false;
					} else {
						this.currentListView.selectHigher();
					}
					return false;
				}
				if(e.keyCode == 40) {
					if(this.currentListView.currentActiveIndex == null) {
						this.currentListView.selectTop();
						return false;
					} else {
						this.currentListView.selectLower();
					}
					return false;
				}

			}

			//Shortcut enter

			if(e.keyCode == 13) {
				this.toggleComposeTweet();
				return false;
			}

			//Shortcut Ctrl + Shift + F

			if(e.keyCode == 70 && e.ctrlKey && e.shiftKey) {
				if(this.search_form.css('display') == 'none') {
					this.search_form.show();
					this.search_form.focus();
				} else {
					this.search_form.hide();
				}
				return false;
			}

			//Shortcut Shift + Tab

			if(e.keyCode == 9 && e.shiftKey) {
				if(this.content_timeline.css('display') == 'block') {
					this.openContent(this.content_mentions,this.mentions_btn);
				} else if(this.content_mentions.css('display') == 'block') {
					this.openContent(this.content_dm,this.dm_btn);
				} else if(this.content_dm.css('display') == 'block') {
					this.openContent(this.content_settings,this.settings_btn);
				} else if(this.content_settings.css('display') == 'block') {
					this.openContent(this.content_timeline,this.timeline_btn);
				}
				return false;
			}
		}
	};

	this.requestPermission = function (callback) {
		window.webkitNotifications.requestPermission(callback);
	};

	this.initializeNotification = function () {
		if(typeof window.webkitNotifications == 'undefined') {
			console.info('이 브라우저는 데스크탑 알림을 지원하지 않습니다.');
		} else {
			if(window.webkitNotifications.checkPermission() > 0) {
				this.requestPermission(this.initializeNotification);
			} else {
				var initMessage = window.webkitNotifications.createNotification('', 'Twift','데스크탑 알림이 활성화 되었습니다.');

				initMessage.show();
				setTimeout(function() {
					initMessage.cancel();
				}, 2000);

				this.webkitNotiStatus = true;
			}
		}
	};

	this.openConversation = function(userid) {
		var self = this;
		this.listviewObject_messageThreads.elementArray.forEach(function(value) {
			if(value.conv_target.id_str == userid) {
				value.messages.forEach(function () {

				});
			}
		});
	};

	this.refreshSettings = function() {
		var self = this;

		this.notifications.css('opacity', this.settings.noti_parent_opacity);

		if(this.settings.ListView_NameFirst == 'nickname') {
			this.listviewObject_timeline.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-first').empty();
				value.listview_jquery.find('.info-name-first').append(value.user.name);
			});
			this.listviewObject_mentions.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-first').empty();
				value.listview_jquery.find('.info-name-first').append(value.user.name);
			});
			this.listviewObject_favorites.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-first').empty();
				value.listview_jquery.find('.info-name-first').append(value.user.name);
			});
		}
		else if(this.settings.ListView_NameFirst == 'screen_name') {
			this.listviewObject_timeline.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-first').empty();
				value.listview_jquery.find('.info-name-first').append('@' + value.user.screen_name);
			});
			this.listviewObject_mentions.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-first').empty();
				value.listview_jquery.find('.info-name-first').append('@' + value.user.screen_name);
			});
			this.listviewObject_favorites.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-first').empty();
				value.listview_jquery.find('.info-name-first').append('@' + value.user.screen_name);
			});
		}
		if(this.settings.ListView_NameSecond == 'nickname') {
			this.listviewObject_timeline.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-second').empty();
				value.listview_jquery.find('.info-name-second').append(value.user.name);
			});
			this.listviewObject_mentions.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-second').empty();
				value.listview_jquery.find('.info-name-second').append(value.user.name);
			});
			this.listviewObject_favorites.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-second').empty();
				value.listview_jquery.find('.info-name-second').append(value.user.name);
			});
		}
		else if(this.settings.ListView_NameSecond == 'screen_name') {
			this.listviewObject_timeline.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-second').empty();
				value.listview_jquery.find('.info-name-second').append('@' + value.user.screen_name);
			});
			this.listviewObject_mentions.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-second').empty();
				value.listview_jquery.find('.info-name-second').append('@' + value.user.screen_name);
			});
			this.listviewObject_favorites.elementArray.forEach(function(value) {
				value.listview_jquery.find('.info-name-second').empty();
				value.listview_jquery.find('.info-name-second').append('@' + value.user.screen_name);
			});
		}
		if(this.settings.ListView_TimeDisplayType == 'relative') {
			this.listviewObject_timeline.elementArray.forEach(function(value) {
				var timestamp = typeof value.retweeted_status !== 'undefined' ? value.retweeted_status.created_at : value.created_at;
				var posixtime = (new Date(Date.parse(timestamp))).getTime();
				var present_time_created = ((new Date()).getTime());
				var seconds = Math.ceil((present_time_created - posixtime) / 1000);
				var minutes = (seconds >= 60) ? Math.ceil(seconds / 60) : seconds;
				var hours = (minutes >= 60) ? Math.ceil(minutes / 60) : minutes;
				var days = (hours >= 24 && minutes >= 60) ? Math.ceil(hours / 24) : hours;

				var time_char = (hours >= 24 && minutes >= 60) ? '일 전' : (minutes >= 60) ? '시간 전' : (seconds >= 60) ? '분 전' : '초 전';

				value.listview_jquery.find('.created-at').empty();
				value.listview_jquery.find('.created-at').append(days + time_char);
			});
			this.listviewObject_mentions.elementArray.forEach(function(value) {
				var timestamp = typeof value.retweeted_status !== 'undefined' ? value.retweeted_status.created_at : value.created_at;
				var posixtime = (new Date(Date.parse(timestamp))).getTime();
				var present_time_created = ((new Date()).getTime());
				var seconds = Math.ceil((present_time_created - posixtime) / 1000);
				var minutes = (seconds >= 60) ? Math.ceil(seconds / 60) : seconds;
				var hours = (minutes >= 60) ? Math.ceil(minutes / 60) : minutes;
				var days = (hours >= 24 && minutes >= 60) ? Math.ceil(hours / 24) : hours;

				var time_char = (hours >= 24 && minutes >= 60) ? '일 전' : (minutes >= 60) ? '시간 전' : (seconds >= 60) ? '분 전' : '초 전';

				value.listview_jquery.find('.created-at').empty();
				value.listview_jquery.find('.created-at').append(days + time_char);
			});
			this.listviewObject_favorites.elementArray.forEach(function(value) {
				var timestamp = typeof value.retweeted_status !== 'undefined' ? value.retweeted_status.created_at : value.created_at;
				var posixtime = (new Date(Date.parse(timestamp))).getTime();
				var present_time_created = ((new Date()).getTime());
				var seconds = Math.ceil((present_time_created - posixtime) / 1000);
				var minutes = (seconds >= 60) ? Math.ceil(seconds / 60) : seconds;
				var hours = (minutes >= 60) ? Math.ceil(minutes / 60) : minutes;
				var days = (hours >= 24 && minutes >= 60) ? Math.ceil(hours / 24) : hours;

				var time_char = (hours >= 24 && minutes >= 60) ? '일 전' : (minutes >= 60) ? '시간 전' : (seconds >= 60) ? '분 전' : '초 전';

				value.listview_jquery.find('.created-at').empty();
				value.listview_jquery.find('.created-at').append(days + time_char);
			});

			self.createRelativeTimer(this.listviewObject_timeline);
			self.createRelativeTimer(this.listviewObject_mentions);
			self.createRelativeTimer(this.listviewObject_favorites);
		}
		else if(this.settings.ListView_TimeDisplayType == 'absolute') {
			self.timerID.forEach(function (value) {
				clearInterval(value);
			});
			self.timerID = [];

			this.listviewObject_timeline.elementArray.forEach(function(value) {
				var timestamp = typeof value.retweeted_status !== 'undefined' ? value.retweeted_status.created_at : value.created_at;
				var absolutetime = window.utils.parseTimestampKorean(timestamp);

				value.listview_jquery.find('.created-at').empty();
				value.listview_jquery.find('.created-at').append(absolutetime.ampm + ' ' + absolutetime.hour + ' ' + absolutetime.min + ' ' + absolutetime.sec);
			});
			this.listviewObject_mentions.elementArray.forEach(function(value) {
				var timestamp = typeof value.retweeted_status !== 'undefined' ? value.retweeted_status.created_at : value.created_at;
				var absolutetime = window.utils.parseTimestampKorean(timestamp);

				value.listview_jquery.find('.created-at').empty();
				value.listview_jquery.find('.created-at').append(absolutetime.ampm + ' ' + absolutetime.hour + ' ' + absolutetime.min + ' ' + absolutetime.sec);
			});
			this.listviewObject_favorites.elementArray.forEach(function(value) {
				var timestamp = typeof value.retweeted_status !== 'undefined' ? value.retweeted_status.created_at : value.created_at;
				var absolutetime = self.utils.parseTimestampKorean(timestamp);

				value.listview_jquery.find('.created-at').empty();
				value.listview_jquery.find('.created-at').append(absolutetime.ampm + ' ' + absolutetime.hour + ' ' + absolutetime.min + ' ' + absolutetime.sec);
			});
		}
		if(this.settings.ilco_profile_image_mode) {
			this.listviewObject_timeline.elementArray.forEach(function(value) {
				value.listview_jquery.find('.profile_image').attr('src','http://abs.twimg.com/sticky/default_profile_images/default_profile_' + window.utils.getRandomInt(0,6).toString() + '_normal.png');
			});
			this.listviewObject_mentions.elementArray.forEach(function(value) {
				value.listview_jquery.find('.profile_image').attr('src','http://abs.twimg.com/sticky/default_profile_images/default_profile_' + window.utils.getRandomInt(0,6).toString() + '_normal.png');
			});
			this.listviewObject_favorites.elementArray.forEach(function(value) {
				value.listview_jquery.find('.profile_image').attr('src','http://abs.twimg.com/sticky/default_profile_images/default_profile_' + window.utils.getRandomInt(0,6).toString() + '_normal.png');
			});
		} else if(!this.settings.ilco_profile_image_mode) {
			this.listviewObject_timeline.elementArray.forEach(function(value) {
				value.listview_jquery.find('.profile_image').attr('src',value.user.profile_image_url_https);
			});
			this.listviewObject_mentions.elementArray.forEach(function(value) {
				value.listview_jquery.find('.profile_image').attr('src',value.user.profile_image_url_https);
			});
			this.listviewObject_favorites.elementArray.forEach(function(value) {
				value.listview_jquery.find('.profile_image').attr('src',value.user.profile_image_url_https);
			});
		}
		if(this.settings.enable_theming) {
			$('#theme_css').remove();
			$(document.head).append('<link rel="stylesheet" href="' + this.settings.theme_css + '" id="theme_css">');
			$('#tmp_css').remove();
			$(document.head).append('<style type="text/css" id="tmp_css">' +
				'.tweet-list-active { background-color: ' + this.settings.selectedColor + '}' +
				'.ReplyToUser { background-color: ' + this.settings.replyToMeColor + '}' +
				'</style>');
		} else if(!this.settings.enable_theming) {
			$('#theme_css').remove();
			$('#tmp_css').remove();
			$(document.head).append('<style type="text/css" id="tmp_css">' +
				'.tweet-list-active { background-color: ' + this.settings.selectedColor + '}' +
				'.ReplyToUser { background-color: ' + this.settings.replyToMeColor + '}' +
				'</style>');
		}
	};

	this.toggleComposeTweet = function (inittext) {
		if(!this.isBlur(this.search_form)) {
			var text = this.search_form.val();
			this.searchTweet(text);
			this.search_form.val('');
			this.search_form.hide();
		} else if(this.compose_form.css('display') == 'none') {
			if(typeof inittext !== 'undefined'){
				this.compose_form_input.val(inittext.toString());
			}
			this.container.css('margin-top', (parseInt(this.container.css('margin-top')) + parseInt(this.compose_form_input.css('height'))));
			this.compose_form.show();
			this.compose_form_input.focus();
		} else {
			if(typeof inittext !== 'undefined'){
				this.search_form.val(inittext.toString());
			}
			if(this.isBlur(this.compose_form_input)) {
				this.compose_form_input.focus();
			} else if(!this.isBlur(this.compose_form_input)) {
				var text = this.compose_form_input.val();
				this.composeTweet(text, this.currentReplyID);
				this.compose_form_input.val('');
				this.container.css('margin-top', (parseInt(this.container.css('margin-top')) - parseInt(this.compose_form_input.css('height'))));
				this.compose_form.hide();
			}
		}
	};

	this.openComposeWithReply = function (id, isSingle) {
		var ReplyUserList = '';
		var self = this;

		if(isSingle) {
			this.currentListView.elementArray.forEach(function (value) {
				if(value.id == id) {
					ReplyUserList += '@' + (typeof value.retweeted_status !== 'undefined' ? value.retweeted_status.user.screen_name : value.user.screen_name) + ' ';
				}
			});
		} else if(!isSingle) {
			this.currentListView.elementArray.forEach(function (value) {
				if(value.id == id) {
					if(value.entities.user_mentions.length == 0) {
						ReplyUserList += '@' + value.user.screen_name + ' ';
					} else {
						ReplyUserList += '@' + value.user.screen_name + ' ';
						value.entities.user_mentions.forEach(function (value) {
							if(value.screen_name !== self.UserInfo.screen_name) ReplyUserList += '@' + value.screen_name + ' ';
						});
					}
				}
			});
		}

		this.currentReplyID = id;
		this.toggleComposeTweet(ReplyUserList);
	};

	this.addDirectMessageListView = function (direct_message) {
		var dmid = direct_message.id_str;
		var time = utils.parseTimestampKorean(direct_message.created_at);
		var isActive = direct_message.sender.id_str == this.UserID;
		var sender_name = direct_message.sender.name;
		var sender_img = direct_message.sender.profile_image_url_https;
		var sender_screen_name = direct_message.sender.screen_name;
		var ListView = this.listviewObject_dm._target_object;
		var html = '';

		if(isActive) html = '<div class="dm-item-self" id="' + this.listviewObject_dm._id_prefix + dmid + '">' +
			'<div class="media dm-user-info">' +
			'<a class="pull-right"><img class="media-object dm-profile" src="' + sender_img + '" /></a>' +
			'<div class="media-body dm-body"><h4 class="media-heading">' + sender_name + '</h4>' + sender_screen_name + '</div>' +
			'</div>' +
			'<span class="dm-time">' + time.year + ' ' + time.month + ' ' + time.date + ' ' + time.day + ' ' + time.ampm + ' ' + time.hour + ' ' + time.min + ' ' + time.sec + '</span>' +
			'<div class="well well-dm">' +
			direct_message.text.replace("\n", "<br>") +
			'</div> ' +
			'<span class="glyphicon glyphicon-chevron-left"></span>' +
			'</div>';
		if(!isActive) html = '<div class="dm-item" id="' + dmid + '">' +
			'<div class="media dm-user-info">' +
			'<a class="pull-left"><img class="media-object dm-profile" src="' + sender_img + '" /></a>' +
			'<div class="media-body dm-body"><h4 class="media-heading">' + sender_name + '</h4>' + sender_screen_name + '</div>' +
			'</div>' +
			'<span class="dm-time">' + time.year + ' ' + time.month + ' ' + time.date + ' ' + time.day + ' ' + time.ampm + ' ' + time.hour + ' ' + time.min + ' ' + time.sec + '</span>' +
			'<div class="well well-dm">' +
			direct_message.text +
			'</div> ' +
			'<span class="glyphicon glyphicon-chevron-right"></span>' +
			'</div>';

		return $(html).appendTo(this.listviewObject_dm._target_object);
	};

	this.addTweetListView = function (parsed, targetListView, isAppending) {

		var self = this;

		/*

		 */

		var isRetweet = typeof parsed.retweeted_status !== 'undefined';
		var real_id = isRetweet ? parsed.retweeted_status.id_str : parsed.id_str;
		var isReplying = parsed.in_reply_to_status_id_str !== null;
		var isReplyingToUser = false;
		var isSelf = (this.UserID == parsed.user.id);

		parsed.entities.user_mentions.forEach(function(value) {
			if(value.id_str == self.UserID) isReplyingToUser = true;
		});

		var id = parsed.id_str;
		var nickname = parsed.user.name;
		var screen_name = parsed.user.screen_name;
		var profile_img_url = parsed.user.profile_image_url_https;
		var text = isRetweet ? parsed.retweeted_status.text : parsed.text;
		var entities = parsed.entities;
		var appname = isRetweet ? parsed.retweeted_status.source : parsed.source;

		var real_text = text;
		text = this.EntitiesFilter(text, entities);
		while(text.indexOf("\n") !== -1) text = text.replace("\n", '<br>');

		var timestamp = isRetweet ? parsed.retweeted_status.created_at : parsed.created_at;
		var posixtime = (new Date(Date.parse(timestamp))).getTime();
		var absolutetime = window.utils.parseTimestampKorean(timestamp);
		var present_time_created = ((new Date()).getTime());
		var seconds = Math.ceil((present_time_created - posixtime) / 1000);
		var minutes = (seconds >= 60) ? Math.ceil(seconds / 60) : seconds;
		var hours = (minutes >= 60) ? Math.ceil(minutes / 60) : minutes;
		var days = (hours >= 24 && minutes >= 60) ? Math.ceil(hours / 24) : hours;
		if(days < 0) days = days + Math.abs(days);
		var time_char = (hours >= 24 && minutes >= 60) ? '일 전' : (minutes >= 60) ? '시간 전' : (seconds >= 60) ? '분 전' : '초 전';

		var timeComplete = this.settings.ListView_TimeDisplayType == 'relative' ?  days + time_char : (this.settings.ListView_TimeDisplayType == 'absolute' ?  absolutetime.ampm + ' ' + absolutetime.hour + ' ' + absolutetime.min + ' ' + absolutetime.sec : '');



		//버튼그륩

		var btn_group;

		if(isRetweet && isSelf) btn_group =
			"<div class=\"btn-group btn-group-justified btn-group-tweetmenu\">" +
				"<div class=\"btn-group\">" +
				"<button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-share-alt\"></span></button>" +
				"<ul class='dropdown-menu' role=\"menu\">" +
				"<li><a class=\"single-reply\">한명에게 멘션</a></li>" +
				"<li><a class=\"multi-reply\">전체 멘션</a></li>" +
				"</ul>" +
				"</div>" +
				"<div class=\"btn-group\">" +
				"<button type=\"button\" class=\"btn btn-warning add-favorite\"><span class=\"glyphicon glyphicon-star\"></span></button>" +
				"</div>" +
				"<div class=\"btn-group\">" +
				"<button type=\"button\" class=\"btn btn-danger remove-tweet\"><span class=\"glyphicon glyphicon-remove\"></span></button>" +
				"</div>" +
				"</div>";
		else if(isSelf) btn_group =
			"<div class=\"btn-group btn-group-justified btn-group-tweetmenu\">" +
				"<div class=\"btn-group\">" +
				"<button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-share-alt\"></span></button>" +
				"<ul class='dropdown-menu' role=\"menu\">" +
				"<li><a class=\"single-reply\">한명에게 멘션</a></li>" +
				"<li><a class=\"multi-reply\">전체 멘션</a></li>" +
				"</ul>" +
				"</div>" +
				"<div class=\"btn-group\">" +
				"<button type=\"button\" class=\"btn btn-success dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-retweet\"></span></button>" +
				"<ul class='dropdown-menu' role=\"menu\">" +
				"<li><a class=\"rt-legacy\">(구)리트윗</a></li>" +
				"<li><a class=\"quote-tweet\">인용하기</a></li>" +
				"</ul>" +
				"</div>" +
				"<div class=\"btn-group\">" +
				"<button type=\"button\" class=\"btn btn-warning add-favorite\"><span class=\"glyphicon glyphicon-star\"></span></button>" +
				"</div>" +
				"<div class=\"btn-group\">" +
				"<button type=\"button\" class=\"btn btn-danger remove-tweet\"><span class=\"glyphicon glyphicon-remove\"></span></button>" +
				"</div>" +
				"</div>";
		else btn_group =
				"<div class=\"btn-group btn-group-justified btn-group-tweetmenu\">" +
					"<div class=\"btn-group\">" +
					"<button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-share-alt\"></span></button>" +
					"<ul class='dropdown-menu' role=\"menu\">" +
					"<li><a class=\"single-reply\">한명에게 멘션</a></li>" +
					"<li><a class=\"multi-reply\">전체 멘션</a></li>" +
					"</ul>" +
					"</div>" +
					"<div class=\"btn-group\">" +
					"<button type=\"button\" class=\"btn btn-success dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-retweet\"></span></button>" +
					"<ul class='dropdown-menu' role=\"menu\">" +
					"<li><a class=\"retweet-tweet\">리트윗</a></li>" +
					"<li><a class=\"rt-legacy\">(구)리트윗</a></li>" +
					"<li><a class=\"quote-tweet\">인용하기</a></li>" +
					"</ul>" +
					"</div>" +
					"<div class=\"btn-group\">" +
					"<button type=\"button\" class=\"btn btn-warning add-favorite\"><span class=\"glyphicon glyphicon-star\"></span></button>" +
					"</div>" +
					"</div>";



		//트윗 작성자 정보

		var name_first;
		var name_second;
		var profile_img;
		var list_view_str;
		var show_replies_button = isReplying ? "<a class=\"show-replies\">대화 표시...</a>" : '';

		if(isRetweet) {
			if(!this.settings.ilco_profile_image_mode) profile_img = profile_img_url;
			else if(this.settings.ilco_profile_image_mode) profile_img = 'http://abs.twimg.com/sticky/default_profile_images/default_profile_' + window.utils.getRandomInt(0,6).toString() + '_normal.png';
			else profile_img = profile_img_url;

			var retweeter_info;

			if(this.settings.ListView_NameFirst == 'nickname') {
				name_first = parsed.retweeted_status.user.name;
				retweeter_info = nickname;
			}
			else if(this.settings.ListView_NameFirst == 'screen_name') {
				name_first = '@' + parsed.retweeted_status.user.screen_name;
				retweeter_info = screen_name;
			}
			else if(this.settings.ListView_NameFirst == 'none') {
				name_first = '';
				retweeter_info = nickname;
			}
			else {
				name_first = parsed.retweeted_status.user.name;
				retweeter_info = nickname;
			}

			if(this.settings.ListView_NameSecond == 'nickname') name_second = parsed.retweeted_status.user.name;
			else if(this.settings.ListView_NameSecond == 'screen_name') name_second = '@' + parsed.retweeted_status.user.screen_name;
			else if(this.settings.ListView_NameSecond == 'none') name_second = '';
			else name_second = '@' + parsed.retweeted_status.user.screen_name;

			list_view_str =
				"<li class=\"list-group-item tweet-list-inactive\" id=\"" + parsed.id_str + "\">" +
					"<img src=\"" + parsed.retweeted_status.user.profile_image_url_https + "\" class=\"profile_image\" />" +
					"<img src=\"" + profile_img + "\" class=\"retweeter-profile-image\" />" +
					"<div class=\"tweet-header\">" +
					"<p class=\"info-name-first\">" + name_first + "</p>" +
					"<p class=\"info-name-second\">" + name_second + "</p>" +
					"</div>" +
					"<div class=\"tweet-body\">" + text + "</div>" +
					"<div class=\"tweet-footer\">" +
					show_replies_button + " <span class=\"created-at\">" + timeComplete + "</span>, via " + appname +
					"</div>" + "<div class='tweet-info'><span class=\"retweeter-info\"><span class=\"glyphicon glyphicon-retweet\"></span> " + retweeter_info + "님이 리트윗</span> <span class='label label-success'>" + parsed.retweeted_status.retweet_count + " RT</span></div> " +
					btn_group + "</li>";

		} else {
			if(!this.settings.ilco_profile_image_mode) profile_img = profile_img_url;
			else if(this.settings.ilco_profile_image_mode) profile_img = 'http://abs.twimg.com/sticky/default_profile_images/default_profile_' + window.utils.getRandomInt(0,6).toString() + '_normal.png';
			else profile_img = profile_img_url;

			if(this.settings.ListView_NameFirst == 'nickname') name_first = nickname;
			else if(this.settings.ListView_NameFirst == 'screen_name') name_first = '@' + screen_name;
			else if(this.settings.ListView_NameFirst == 'none') name_first = '';
			else name_first = nickname;

			if(this.settings.ListView_NameSecond == 'nickname') name_second = nickname;
			else if(this.settings.ListView_NameSecond == 'screen_name') name_second = '@' + screen_name;
			else if(this.settings.ListView_NameSecond == 'none') name_second = '';
			else name_second = '@' + screen_name;

			list_view_str =
				"<li class=\"list-group-item tweet-list-inactive\" id=\"" + parsed.id_str + "\">" +
					"<img src=\"" + profile_img + "\" class=\"profile_image\"/>" +
					"<div class=\"tweet-header\">" +
					"<p class=\"info-name-first\">" + name_first + "</p>" +
					"<p class=\"info-name-second\">" + name_second + "</p>" +
					"</div>" +
					"<div class=\"tweet-body\">" + text + "</div>" +
					"<div class=\"tweet-footer\">" +
					show_replies_button + " <span class=\"created-at\">" + timeComplete + "</span>, via " + appname + "</div>" +
					"<div class='tweet-info'></div>" +
					btn_group + "</li>";
		}

		//리스트에 삽입

		var tweet_information = {
			created_at: parsed.created_at,
			id: id,
			id_str: parsed.id_str,
			id_int: parsed.id,
			real_id: real_id,
			text: real_text,
			user: parsed.user,
			retweeted_status: parsed.retweeted_status,
			entities: entities,
			deleted: false
		};

		var created_elem;

		if(isAppending) created_elem = targetListView.appendListView(list_view_str, tweet_information);
		else if(!isAppending) created_elem = targetListView.prependListView(list_view_str, tweet_information);

		if(isReplyingToUser) created_elem.addClass('ReplyToUser');
		created_elem.find('.tweet-footer').find('a').attr('target','_blank');

		if(targetListView.elementArray.length >= 3) {
			var prevElem = targetListView.elementArray[targetListView.elementArray.length - 2].listview_jquery;
			var prevElemBottomOffset = (this.compose_form.css('display') == 'table') ? pageYOffset + this.navbar.height() + this.compose_form.outerHeight() : pageYOffset + this.navbar.height() ;
			var totalNavbarHeight = (this.compose_form.css('display') == 'table') ? prevElem.offset().top + prevElem.outerHeight() : prevElem.outerHeight();
			if(prevElemBottomOffset <= totalNavbarHeight) {
				created_elem.hide();
				created_elem.slideDown();
				scrollTo(0, pageYOffset - 1);
			} else if(targetListView !== this.currentListView) {
				targetListView.lastYOffset = targetListView.lastYOffset + created_elem.outerHeight() - 1;
			} else if(targetListView == this.currentListView) {
				scrollTo(0, pageYOffset + created_elem.outerHeight() - 1);
			}
		} else {
			created_elem.hide();
			created_elem.slideDown();
			scrollTo(0, pageYOffset - 1);
		}

		created_elem.click(function (e) {
			var jqElem = $(e.currentTarget);

			if(!self.currentListView.isActiveListView($(jqElem))) {
				self.currentListView.setActiveListView($(jqElem));
			}
		});

		$(created_elem.find('.btn-group.btn-group-justified.btn-group-tweetmenu')).find('.single-reply').click(function() {
			self.openComposeWithReply(id, true);
		});
		$(created_elem.find('.btn-group.btn-group-justified.btn-group-tweetmenu')).find('.multi-reply').click(function() {
			self.openComposeWithReply(id, false);
		});
		$(created_elem.find('.btn-group.btn-group-justified.btn-group-tweetmenu')).find('.retweet-tweet').click(function() {
			self.retweetTweet(id);
		});
		$(created_elem.find('.btn-group.btn-group-justified.btn-group-tweetmenu')).find('.rt-legacy').click(function() {
			self.legacyRetweet(self.currentListView ,id);
		});
		$(created_elem.find('.btn-group.btn-group-justified.btn-group-tweetmenu')).find('.quote-tweet').click(function() {
			self.quoteTweet(self.currentListView, id);
		});
		$(created_elem.find('.btn-group.btn-group-justified.btn-group-tweetmenu')).find('.add-favorite').click(function() {
			self.favoriteTweet(id);
		});
		$(created_elem.find('.btn-group.btn-group-justified.btn-group-tweetmenu')).find('.remove-tweet').click(function() {
			self.deleteTweet(id);
		});

		var i = 0;

		if(entities.hashtags.length !== 0) {
			var hashtags_objects = created_elem.find('.tweet-body > .hashtags');
			for(i = 0; i < hashtags_objects.length ; i++) {
				$(hashtags_objects[i]).click(function (e) {
					var currentTarget = $(e.currentTarget);
					console.log(currentTarget.attr('data-real'));
				});
			}
		}

		if(entities.symbols.length !== 0) {
			var symbols_objects = created_elem.find('.tweet-body > .symbols');
			for(i = 0; i < symbols_objects.length ; i++) {
				$(symbols_objects[i]).click(function (e) {
					var currentTarget = $(e.currentTarget);
					console.log(currentTarget.attr('data-real'));
				});
			}
		}

		if(entities.user_mentions.length !== 0) {
			var user_mentions_objects = created_elem.find('.tweet-body > .user-mentions');
			for(i = 0; i < user_mentions_objects.length ; i++) {
				$(user_mentions_objects[i]).click(function (e) {
					var currentTarget = $(e.currentTarget);
					console.log(currentTarget.attr('data-real'));
				});
			}
		}

		if(typeof entities.media !== 'undefined') {
			var media_objects = created_elem.find('.tweet-body > .media');
			for(i = 0; i < media_objects.length ; i++) {
				$(media_objects[i]).click(function (e) {
					var currentTarget = $(e.currentTarget);

					self.modal_body_content.empty();
					self.modal_body_content.append('<img src="' + currentTarget.attr('data-real') + '" class="real-image" />');

					self.currentMedia = currentTarget.html();

					self.modal_default_openin_twitter.click(function(e) {
						window.open('https://' + self.currentMedia, '_blank');
					});

					self.modal_default.modal({keybord: true, show: true});
				});
			}
		}
	};

	this.EntitiesFilter = function (text, entities) {
		var return_text = text;
		var hashtags = entities.hashtags;
		var symbols = entities.symbols;
		var urls = entities.urls;
		var user_mentions = entities.user_mentions;
		var media = entities.media;

		if(hashtags.length !== 0) {
			hashtags.forEach(function (value) {
				return_text = return_text.replace('#' + value.text, '<a class="hashtags" data-real="' + value.text + '">#' + value.text +'</a>');
			});
		}

		if(symbols.length !== 0) {
			symbols.forEach(function (value) {
				return_text = return_text.replace('$' + value.text, '<a class="symbols" data-real="' + value.text + '">$' + value.text +'</a>');
			});
		}


		if(urls.length !== 0) {
			urls.forEach(function (value) {
				return_text = return_text.replace(value.url, '<a href="' + value.expanded_url + '" target="_blank" class="urls" data-real="' + value.expanded_url + '">' + value.display_url +'</a>');
			});
		}

		if(user_mentions.length !== 0) {
			user_mentions.forEach(function (value) {
				return_text = return_text.replace('@' + value.screen_name, '<a href="https://www.twitter.com/' + value.screen_name + '" target="_blank" class="user-mentions" data-real="' + value.screen_name + '">@' + value.screen_name +'</a>');
			});
		}

		if(typeof entities.media !== 'undefined') {
			media.forEach(function (value) {
				return_text = return_text.replace(value.url, '<a class="media" data-real="' + value.media_url_https + '" >' + value.display_url +'</a>');
			});
		}

		return return_text;
	};

	this.legacyRetweet = function (targetListView, id) {
		var text = undefined;

		targetListView.elementArray.forEach(function (value) {
			if(value.id == id) {
				text = 'RT @' + value.user.screen_name + ': ' + value.text;
			}
		});

		if(typeof text !== 'undefined') {
			this.currentReplyID = id;
			this.toggleComposeTweet(text);
		}
	};

	this.quoteTweet = function (targetListView, id) {
		var text = undefined;

		targetListView.elementArray.forEach(function (value) {
			if(value.id == id) {
				text = '"@' + value.user.screen_name + ': ' + value.text + '"';
			}
		});

		if(typeof text !== 'undefined') {
			this.currentReplyID = id;
			this.toggleComposeTweet(text);
		}
	};

	this.createRelativeTimer = function (targetListView) {
		this.timerID.push(setInterval(function () {
			targetListView.elementArray.forEach(function (value) {
				var created_at = (typeof value.retweeted_status == 'undefined') ? value.created_at : value.retweeted_status.created_at;
				var posixtime = (new Date(Date.parse(created_at))).getTime();
				var present_time = (new Date()).getTime();

				var seconds = Math.ceil((present_time - posixtime) / 1000);
				var minutes = (seconds >= 60) ? Math.ceil(seconds / 60) : seconds;
				var hours = (minutes >= 60) ? Math.ceil(minutes / 60) : minutes;
				var days = (hours >= 24 && minutes >= 60) ? Math.ceil(hours / 24) : hours;

				var time_char = (hours >= 24 && minutes >= 60) ? '일 전' : (minutes >= 60) ? '시간 전' : (seconds >= 60) ? '분 전' : '초 전';
				var time_span = $(value.listview_jquery.find('.created-at'));

				time_span.empty();
				time_span.append(days + time_char);
			})
		},1000));
	};

	this.openContent = function (JqueryObject, clickedObject) {

		var self = this;
		var YOffset = pageYOffset;
		if(this.currentListView !== null) {
			this.currentListView.lastYOffset = YOffset;
		}


		this.ClsSel.contents.hide();
		this.ClsSel.nav_activable.removeClass('active');
		clickedObject.addClass('active');
		JqueryObject.show();

		if(JqueryObject == this.content_timeline) {
			this.content_mode = 0;
			this.currentListView = this.listviewObject_timeline;
			this.timeline_badge.empty();
			this.enableKeyboardEvent = true;
		}
		if(JqueryObject == this.content_mentions){
			this.content_mode = 1;
			this.currentListView = this.listviewObject_mentions;
			this.mentions_badge.empty();
			this.enableKeyboardEvent = true;
		}
		if(JqueryObject == this.content_favorites) {
			this.content_mode = 2;
			this.currentListView = this.listviewObject_favorites;
			this.enableKeyboardEvent = true;
		}
		if(JqueryObject == this.content_dm) {
			this.content_mode = 3;
			this.currentListView = this.listviewObject_messageThreads;
			this.enableKeyboardEvent = false;
		}
		if(JqueryObject == this.content_settings) {
			this.content_mode = 4;
			this.currentListView = null;
			this.enableKeyboardEvent = false;
		}

		scrollTo(0, this.currentListView.lastYOffset);

		return true;
	};

	this.isBlur = function (JqueryObject) {
		var string = JqueryObject.attr('isblur');
		return (string == 'true');
	};

	this.createWebkitNotification = function(thumb, title, text, timeout) {
		if(this.webkitNotiStatus) {
			var webkitMessage = window.webkitNotifications.createNotification(thumb, title, text);
			webkitMessage.show();

			setTimeout(function() {
				webkitMessage.cancel();
			}, timeout);
		}
	};

	this.createNotification = function(type, text, timeout) {
		var noti_obj = $("<div class=\"alert alert-" + type + "\">" + text + "</div>").prependTo(this.notifications);

		noti_obj.hide();
		noti_obj.slideDown();
		noti_obj.css('opacity', 0);
		noti_obj.animate({
			opacity: 1.0
		});

		setTimeout(function() {
			noti_obj.animate({
				opacity: 0
			},400,function () {
				noti_obj.remove();
			});
		}, timeout);
	};

	this.createNotificationPanel = function(type, title, text, timeout) {
		var noti_obj = $(
			"<div class=\"panel panel-" + type + "\">" +
				"<div class='panel-heading'>" +
				"<h3 class='panel-title'>" + title + "</h3>" +
				"</div>" +
				"<div class='panel-body'>" + text + "</div>" +
				"</div>").prependTo(this.notifications);

		noti_obj.hide();
		noti_obj.slideDown();
		noti_obj.css('opacity', 0);
		noti_obj.animate({
			opacity: 1.0
		});

		setTimeout(function() {
			noti_obj.animate({
				opacity: 0
			}, 400, function () {
				noti_obj.remove();
			});
		}, timeout);
	};
}

$(document).ready(function () {
	window.pageManager = new Manager();
	pageManager.init();
	pageManager.initializeNotification();
});