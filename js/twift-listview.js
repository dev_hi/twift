/**
 * Created by Home on 14. 3. 26.
 */

var ListView = function (target_object, id_prefix, listviewActiveClassName, listviewInactiveClassName) {
	this._this = this;
	this._target_object = target_object;
	this._id_prefix = id_prefix;
	this._listviewActiveClassName = listviewActiveClassName; //'tweet-list-active';
	this._listviewInactiveClassName = listviewInactiveClassName; //'tweet-list-inactive';

	this.elementArray = [];
	this.currentActiveIndex = null;
	this.firstIndex = 0;
	this.lastIndex = 0;
	this.lastYOffset = 0;
};

ListView.prototype = {
	appendListView: function (html, obj) {
		var JQobject = $(html).appendTo(this._target_object);
		JQobject.attr('id', this._id_prefix + JQobject.attr('id'));
		obj.listview_jquery = JQobject;
		obj.listid = obj.listview_jquery.attr('id');
		this.elementArray.unshift(obj);
		this.firstIndex = this.firstIndex + 1;
		this.currentActiveIndex = (this.currentActiveIndex == null) ? null : this.currentActiveIndex + 1;

		return JQobject;
	},
	prependListView: function (html, obj) {
		var JQobject = $(html).prependTo(this._target_object);
		JQobject.attr('id', this._id_prefix + JQobject.attr('id'));
		obj.listview_jquery = JQobject;
		obj.listid = obj.listview_jquery.attr('id');
		this.elementArray.push(obj);
		this.lastIndex = this.lastIndex + 1;

		return JQobject;
	},
	deleteListView: function () {

	},
	unActiveAllListView: function () {
		var self = this;
		var Active = this._target_object.find('.' + this._listviewActiveClassName);
		Active.forEach(function (value) {
			$(value).removeClass(self._listviewActiveClassName);
			$(value).addClass(self._listviewInactiveClassName);
		});
		this.currentActiveIndex = null;
	},
	isActiveListView: function (jqElem) {
		return jqElem.hasClass(this._listviewActiveClassName);
	},
	setActiveListView: function (jqElem) {
		var self = this;
		if(jqElem.length > 0) {
			var prevActive = this._target_object.find('.' + this._listviewActiveClassName);

			if(prevActive.length > 0) {
				$(prevActive[0]).removeClass(this._listviewActiveClassName);
				$(prevActive[0]).addClass(this._listviewInactiveClassName);
			}

			jqElem.removeClass(this._listviewInactiveClassName);
			jqElem.addClass(this._listviewActiveClassName);

			var jqElem_ID = jqElem.attr('id');

			this.elementArray.forEach(function (value, key) {
				if(value.listid == jqElem_ID) {
					self.currentActiveIndex = key;
				}
			});

			if(jqElem.offset().top <= pageYOffset + $('#navbar').height()) window.scrollTo(0, jqElem.offset().top - 50); //pageYOffset - jqElem.outerHeight()
			if(jqElem.offset().top + jqElem.outerHeight() >= pageYOffset + $(window).height()) window.scrollTo(0, (jqElem.offset().top - $(window).height()) + jqElem.outerHeight());

			return true;
		} else {
			console.warn('선택할 트윗이 없습니다.');
			return false;
		}
	},
	findListViewByIndex: function (index) {
		if(index < this.elementArray.length && index >= 0) {
			return jqElem = this.elementArray[index].listview_jquery;
		} else {
			return false;
		}
	},
	selectTop: function() {
		this.setActiveListView(this.findListViewByIndex(this.elementArray.length - 1));
	},
	selectHigher: function () {
		this.setActiveListView(this.findListViewByIndex(this.currentActiveIndex + 1));
	},
	selectLower: function () {
		this.setActiveListView(this.findListViewByIndex(this.currentActiveIndex - 1));
	},
	selectBottom: function () {
		this.setActiveListView(this.findListViewByIndex(0));
	},
	initialize: function () {
		this._target_object.empty();
		this.elementArray = [];
		this.currentActiveIndex = null;
		this.firstIndex = 0;
		this.lastIndex = 0;
		this.lastYOffset = 0;
	}
};